package kame.api;

import java.util.Arrays;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import kame.api.baseapi.Actionbar;
import kame.api.position.Teleport;
import kame.api.position.Teleport.TeleportFlag;
import kame.kameplayer.baseutils.Utils;
import kame.kameplayer.event.ItemDamageEvent;

public class KameAPI {

	private KameAPI() {}
	
	/**
	 * 文字列からItemStackを作成する
	 * <li><b>type:1</b><br>
	 * Material:Count:Damage<br>
	 * Material Count Damage
	 * <li><b>type:2</b><br>
	 * Material:Damage:Count<br>
	 * Material Damage Count
	 * 
	 * @param input
	 * @param type
	 * @return
	 */
	public static ItemStack parseItemStack(String input, int type) {
		String str[] = input.replace(':', ' ').split(" ");
		@SuppressWarnings("deprecation")
		Material mate = Bukkit.getUnsafe().getMaterialFromInternalName(str[0]);
		if(mate == Material.AIR)mate = Material.matchMaterial(str[0]);
		if(mate == null)return null;
		return new ItemStack(mate,
				str.length > 1 ? Utils.parseUnsignedInt(str[1] , 1) : 1,
		(short)(str.length > 2 ? Utils.parseUnsignedInt(str[2] , 0) : 0));
	}
	
	public static MaterialData parseMaterialData(String str) {
		return null;
	}
	
	public static Material parseMaterial(String str) {
		return null;
	}
	
	public static void sendActionbar(Player player, String message) {
		Actionbar.sendMessage(player, message);
	}

	public static void teleport(Player player, Location loc, Set<TeleportFlag> flag) {
		Teleport.teleport(player, loc, flag);
	}

	public static Location getPlayerEye(Player player) {
		try {
			if(Bukkit.getPluginManager().isPluginEnabled("MorePlayerModels2")) {
				noppes.mpm.ModelData data = noppes.mpm.ModelData.get(player);
				return player.getLocation().add(0, ((data.body.scaleY + data.leg1.scaleY) / 2)
						* player.getEyeHeight(), 0);
			}
		}catch (NoSuchMethodError e) {}
		return player.getEyeLocation();
	}

	private static Pattern p = Pattern.compile("(durability ?: ?)([0-9]+)( ?/ ?)([0-9]+)");

	public static Result itemDamage(Player player, ItemStack item, int damage) {
		if(item == null || item.getAmount() == 0) return Result.DEFAULT;
		ItemMeta im = item.getItemMeta();
		if(!im.hasLore()) return Result.DEFAULT;
		String lore = String.join("\n", im.getLore());
		Matcher m = p.matcher(lore.toLowerCase());
		if(m.find()) {
			Integer ench = im.getEnchants().get(Enchantment.DURABILITY);
			if(ench != null)
				damage /= 1 + Math.random() * (ench + 1);
			ItemDamageEvent event = new ItemDamageEvent(player, item, damage);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled() || event.getDamage() == 0)return Result.ALLOW;
			String line = m.group(1);
			int durability = Integer.parseInt(m.group(2));
			durability -= event.getDamage();
			lore = m.replaceFirst(line + durability + m.group(3) + m.group(4));
			im.setLore(Arrays.asList(lore.split("\n")));
			item.setItemMeta(im);
			return durability < 0 ? Result.DENY : Result.ALLOW;
		}
		return Result.DEFAULT;
	}

}
