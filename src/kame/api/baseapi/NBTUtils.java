package kame.api.baseapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

@Deprecated
@SuppressWarnings("unchecked")
public class NBTUtils {

	private NBTUtils() {}

	public Object getNBTTag(Object nbt_or_item) {
		return Vx_x_Rx.get().getNBTTag(nbt_or_item);
	}

	public static Set<String> getNBTKeys(Entity entity) {
		return Vx_x_Rx.get().getNBTKeys(entity);
	}

	public static Set<String> getNBTKeys(ItemStack item) {
		return Vx_x_Rx.get().getNBTKeys(item);
	}

	public static Set<String> getNBTKeys(Block block) {
		return Vx_x_Rx.get().getNBTKeys(block);
	}

	public static Object getNBTTag(Entity entity, String tagname) {
		return Vx_x_Rx.get().getNBTTag(entity, tagname);
	}

	public static Object getNBTTag(ItemStack item, String tagname) {
		return Vx_x_Rx.get().getNBTTag(item, tagname);
	}

	public static Object getNBTTag(Block block, String tagname) {
		return Vx_x_Rx.get().getNBTTag(block, tagname);
	}

	public static String parseNBT(Map<String, Object> mbtmap) {
		return Vx_x_Rx.get().parseNBT(mbtmap);
	}

	public static Map<String, Object> parseNBT(String nbttag) {
		return Vx_x_Rx.get().parseNBT(nbttag);
	}

	public static void setNBTTag(Entity entity, String tagname, Object nbt) {
		Vx_x_Rx.get().setNBTTag(entity, tagname, nbt);
	}

	public static void setNBTTag(ItemStack item, String tagname, Object nbt) {
		Vx_x_Rx.get().setNBTTag(item, tagname, nbt);
	}

	public static void setNBTTag(Block block, String tagname, Object nbt) {
		Vx_x_Rx.get().setNBTTag(block, tagname, nbt);
	}

	public static Map<String, Object> getMap(Object nbtObject) {
		return nbtObject instanceof Map ? (Map<String, Object>) nbtObject : new HashMap<>();
	}

	public static List<Object> getList(Object nbtObject) {
		return nbtObject instanceof List ? (List<Object>) nbtObject : new ArrayList<>();
	}

	public static Number getNumber(Object nbtObject, Number defaultNumber) {
		return nbtObject instanceof Number ? (Number) nbtObject : defaultNumber;
	}

	public static String getString(Object nbtObject, String defaultString) {
		return nbtObject instanceof Number || nbtObject instanceof String ? String.valueOf(nbtObject) : defaultString;
	}

	public static Map<String, Object> toMap(ItemStack item) {
		Map<String, Object> map = new HashMap<>();
		map.put("id", Vx_x_Rx.get().getItemMinecraft(item));
		map.put("Damage", item.getDurability());
		map.put("Count", (short) item.getAmount());
		Map<String, Object> nbt = new HashMap<>();
		getNBTKeys(item).forEach(key -> nbt.put(key, getNBTTag(item, key)));
		if(!nbt.isEmpty())
			map.put("nbt", nbt);
		return map;
	}

	public static ItemStack createItemStack(Map<String, Object> map) {
		ItemStack item = Vx_x_Rx.get().getItemMinecraft(getString(map.get("id"), "minecraft:air"));
		item.setDurability(getNumber(map.get("Damage"), 0).shortValue());
		item.setAmount(getNumber(map.get("Count"), 1).intValue());
		getMap(map.get("nbt")).entrySet().forEach(x -> setNBTTag(item, x.getKey(), x.getValue()));
		return item;
	}
}
