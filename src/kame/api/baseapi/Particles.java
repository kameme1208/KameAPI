package kame.api.baseapi;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

@Deprecated
public class Particles {

	private Particles() {}

	private static Map<String, Object> particles = new TreeMap<>();

	private static void init() {
		Vx_x_Rx.get().putParticles(particles);
		particles = Collections.unmodifiableMap(particles);
	}

	public static Set<String> getParticles() {
		if(particles.isEmpty())
			init();
		return particles.keySet();
	}

	public static Object getParticle(String particle) {
		if(particles.isEmpty())
			init();
		return particles.get(particle);
	}

	public static void sendParticle(Player player, Particle particle, Location loc, Vector vec, int amount, double speed, double radius, boolean force, int... data) {
		sendParticle(player, particle.toString(), loc, vec, amount, speed, radius, force, data);
	}

	public static void sendParticle(Player player, String particlename, Location loc, Vector vec, int amount, double speed, double radius, boolean force, int... data) {
		particlename = particlename.toLowerCase();
		if(particles.isEmpty())
			init();
		if(!particles.containsKey(particlename))
			throw new IllegalArgumentException(particlename);
		Object particle = particles.get(particlename);
		if(player.getWorld().equals(loc.getWorld()) && player.getLocation().distance(loc) <= radius)
			Vx_x_Rx.get().sendParticle(player, particle, (float) loc.getX(), (float) loc.getY(), (float) loc.getZ(), (float) vec.getX(), (float) vec.getY(), (float) vec.getZ(), (float) speed, amount, force, data);
	}

	public enum Particle {
		angryvillager,
		barrier,
		blockcrack,
		blockdust,
		bubble,
		cloud,
		crit,
		damageindicator,
		depthsuspend,
		dragonbreath,
		driplava,
		dripwater,
		droplet,
		enchantmenttable,
		endrod,
		explode,
		fallingdust,
		fireworksspark,
		flame,
		footstep,
		happyvillager,
		heart,
		hugeexplosion,
		iconcrack,
		instantspell,
		largeexplode,
		largesmoke,
		lava,
		magiccrit,
		mobappearance,
		mobspell,
		mobspellambient,
		note,
		portal,
		reddust,
		slime,
		smoke,
		snowballpoof,
		snowshovel,
		spell,
		splash,
		suspended,
		sweepattack,
		take,
		townaura,
		wake,
		witchmagic,

	}
}