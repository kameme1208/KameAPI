package kame.api.baseapi;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import kame.api.collision.CollisionPosition;
import kame.api.position.Teleport.TeleportFlag;

public abstract class Vx_x_Rx {
	public static Vx_x_Rx get() {
		return THIS;
	}

	private static Vx_x_Rx THIS;

	public static void init(Vx_x_Rx version) {
		if(THIS == null)
			THIS = version;
	}

	public abstract void sendPacket(Player player, String packet, float x, float y, float z, float x2, float y2, float z2, float speed, int amount);

	public abstract void sendParticle(Player player, Object particle, float x, float y, float z, float dx, float dy, float dz, float speed, int amount, boolean force, int... data);

	public abstract void sendRespawn(Player player);

	public abstract void sendBarMessage(Player player, String message);

	public abstract Entity getNBTEntity(CommandSender sender, Location loc, String[] args);

	public abstract void copyBlockEntity(World world, int x, int y, int z, int i, int j, int k);

	public abstract void copyBlockCommandNBT(Block b, byte power);

	public abstract void putParticles(Map<String, Object> particles);

	public abstract String getNBTTag(ItemStack bufferitem);

	public abstract Object getNBTTag(ItemStack item, String tagname);

	public abstract Object getNBTTag(Entity entity, String tagname);

	public abstract Object getNBTTag(Block block, String tagname);

	public abstract Set<String> getNBTKeys(ItemStack item);

	public abstract Set<String> getNBTKeys(Entity entity);

	public abstract Set<String> getNBTKeys(Block block);

	public abstract void setNBTTag(ItemStack item, String tagname, Object nbt);

	public abstract void setNBTTag(Entity entity, String tagname, Object nbt);

	public abstract void setNBTTag(Block block, String tagname, Object nbt);

	public abstract Map<String, Object> parseNBT(String nbttag);

	public abstract String parseNBT(Map<String, Object> nbtmap);

	public abstract Set<String> getNBTTagCompoundKeys(Object tag);

	public abstract Object getNBTTagCompoundObject(Object tag, String tagname);

	public abstract Object getNBTTag(Object nbt);

	public abstract Object parseNBTTag(Object nbt);

	public abstract Class<?> getNBTType(Object nbt);

	public abstract void setNBTTagCompoundObject(Object tag, String tagname, Object settag);

	public abstract Object comple(ItemStack bufferitem);

	public abstract Object comple(Entity entity);

	public abstract Object comple(Block block);

	public abstract Object read(ItemStack editor, Object item);

	public abstract Object read(Entity editor, Object entity);

	public abstract Object read(Block editor, Object block);

	public abstract <T> T buildItem(Object item, Object tag);

	public abstract <T> T buildEntity(Object entity, Object tag);

	public abstract void buildBlock(Object block, Object tag);

	public abstract String getItemMinecraft(ItemStack item);

	public abstract ItemStack getItemMinecraft(String item);

	public abstract CollisionPosition rayTrace(Location start, Location end, boolean liquid, boolean transpare, boolean returnuncollidable);

	public abstract CollisionPosition rayTrace(Location start, Location end, Vector box1, Vector box2);

	public abstract CollisionPosition rayTraceEntity(Location start, Location end, Collection<Entity> entities);

	public abstract CollisionPosition rayTraceWorld(Location start, Location end, Collection<Entity> entities, boolean liquid, boolean transpare, boolean returnuncollidable);

	public abstract List<CollisionPosition> rayTraceWorld(Location start, Location end, Collection<Entity> entities, boolean liquid, boolean transpare, boolean returnuncollidable, Class<? extends Entity> filter);

	public abstract void teleport(Player player, Location loc, Set<TeleportFlag> flag);
}
