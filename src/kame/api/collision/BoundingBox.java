package kame.api.collision;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

public class BoundingBox {

	private final World world;

	public double	locX;
	public double	locY;
	public double	locZ;

	private double	Xmin;
	private double	Xmax;

	private double	Ymin;
	private double	Ymax;

	private double	Zmin;
	private double	Zmax;

	public double	yaw;
	public double	pitch;
	public double	roll;

	private final Consumer<Vector> rotateYaw = new Consumer<Vector>() {
		public void accept(Vector vec) {
			if(yaw == 0)
				return;
			double old = vec.getX();
			double sin = Math.sin(yaw);
			double cos = Math.cos(yaw);
			vec.setX(old * cos - vec.getZ() * sin);
			vec.setZ(old * sin + vec.getZ() * cos);
		}
	};

	private final Consumer<Vector> rotatePitch = new Consumer<Vector>() {
		public void accept(Vector vec) {
			if(pitch == 0)
				return;
			double old = vec.getZ();
			double sin = Math.sin(-pitch);
			double cos = Math.cos(-pitch);
			vec.setZ(old * cos - vec.getY() * sin);
			vec.setY(old * sin + vec.getY() * cos);
		}
	};

	private final Consumer<Vector> rotateRoll = new Consumer<Vector>() {
		public void accept(Vector vec) {
			if(roll == 0)
				return;
			double old = vec.getX();
			double sin = Math.sin(roll);
			double cos = Math.cos(roll);
			vec.setX(old * cos - vec.getY() * sin);
			vec.setY(old * sin + vec.getY() * cos);
		}
	};

	private final List<Consumer<Vector>> rank = new ArrayList<Consumer<Vector>>(3);

	public BoundingBox(@Nullable World world, Vector center, Vector box1, Vector box2) {
		this.world = world;
		locX = center.getX();
		locY = center.getY();
		locZ = center.getZ();
		double min;
		double max;
		min = box1.getX() - locX;
		max = box2.getX() - locX;
		this.Xmin = Math.min(min, max);
		this.Xmax = Math.max(min, max);

		min = box1.getY() - locY;
		max = box2.getY() - locY;
		this.Ymin = Math.min(min, max);
		this.Ymax = Math.max(min, max);

		min = box1.getZ() - locZ;
		max = box2.getZ() - locZ;
		this.Zmin = Math.min(min, max);
		this.Zmax = Math.max(min, max);
		rank.add(0, rotateYaw);
		rank.add(1, rotatePitch);
		rank.add(2, rotateRoll);
	}

	public BoundingBox(@Nullable World world, Vector box1, Vector box2) {
		this(world, box1.getMidpoint(box2), box1, box2);
	}

	public BoundingBox(Location center, Location box1, Location box2) {
		this(box1.getWorld(), center.toVector(), box1.toVector(), box2.toVector());
	}

	public BoundingBox(Location box1, Location box2) {
		this(box1.getWorld(), box1.toVector(), box2.toVector());
	}

	public HitResult rayTrace(Location start, Location end) {
		return rayTrace(start.toVector(), end.toVector());
	}

	public HitResult rayTrace(Vector start, Vector end) {
		if(locX != 0 || locY != 0 || locZ != 0 || yaw != 0 || pitch != 0 || roll != 0) {
			start = start.clone();
			end = end.clone();
			if(locX != 0) {
				start.setX(start.getX() - locX);
				end.setX(end.getX() - locX);
			}
			if(locY != 0) {
				start.setY(start.getY() - locY);
				end.setY(end.getY() - locY);
			}
			if(locZ != 0) {
				start.setZ(start.getZ() - locZ);
				end.setZ(end.getZ() - locZ);
			}
			pitch = -pitch;
			yaw = -yaw;
			roll = -roll;
			for(Consumer<Vector> roll : rank)roll.accept(start);
			for(Consumer<Vector> roll : rank)roll.accept(end);
			pitch = -pitch;
			yaw = -yaw;
			roll = -roll;
		}
		Vector vec = null;
		Vector loc = null;
		BlockFace face = BlockFace.SELF;
		if(start.getX() < end.getX()) {
			loc = getX(start, end, this.Xmin);
			if(neko(start, vec, loc)) {
				vec = loc;
				face = BlockFace.WEST;
			}
		}else if(start.getX() > end.getX()) {
			loc = getX(start, end, this.Xmax);
			if(neko(start, vec, loc)) {
				vec = loc;
				face = BlockFace.EAST;
			}
		}
		if(start.getY() < end.getY()) {
			loc = getY(start, end, this.Ymin);
			if(neko(start, vec, loc)) {
				vec = loc;
				face = BlockFace.DOWN;
			}
		}else if(start.getY() > end.getY()) {
			loc = getY(start, end, this.Ymax);
			if(neko(start, vec, loc)) {
				vec = loc;
				face = BlockFace.UP;
			}
		}
		if(start.getZ() < end.getZ()) {
			loc = getZ(start, end, this.Zmin);
			if(neko(start, vec, loc)) {
				vec = loc;
				face = BlockFace.NORTH;
			}
		}else if(start.getZ() > end.getZ()) {
			loc = getZ(start, end, this.Zmax);
			if(neko(start, vec, loc)) {
				vec = loc;
				face = BlockFace.SOUTH;
			}
		}
		if(vec == null && start.getX() < Xmax && start.getX() > Xmin && start.getY() < Ymax && start.getY() > Ymin
				&& start.getZ() < Zmax && start.getZ() > Zmin) {
			vec = start;
		}
		return new HitResult(face, vec);
	}

	public Location getWorldPosition(HitResult result) {
		return roll(result.hitpos).toLocation(world).add(locX, locY, locZ);
	}

	public Location getBoxPosition(HitResult result) {
		return result.hitpos.toLocation(world);
	}

	public Vector getCenter(Vector vec) {
		return vec.setX(locX).setY(locY).setZ(locZ);
	}

	public void setRank(Rank master, Rank slave) {
		if(master == slave)
			throw new IllegalArgumentException("Master and slave must be different");
		switch (master) {
		case YAW:
			rank.set(0, rotateYaw);
			rank.set(1, slave == Rank.PITCH ? rotatePitch : rotateRoll);
			rank.set(2, slave == Rank.PITCH ? rotateRoll : rotatePitch);
			return;
		case PITCH:
			rank.set(0, rotatePitch);
			rank.set(1, slave == Rank.ROLL ? rotateRoll : rotateYaw);
			rank.set(2, slave == Rank.ROLL ? rotateYaw : rotateRoll);
			return;
		case ROLL:
			rank.set(0, rotateRoll);
			rank.set(1, slave == Rank.YAW ? rotateYaw : rotatePitch);
			rank.set(2, slave == Rank.YAW ? rotatePitch : rotateYaw);
			return;
		}
	}

	public Location getCenter(Location loc) {
		loc.zero().add(locX, locY, locZ);
		loc.setDirection(roll(new Vector(0, 0, 1)));
		return loc;
	}

	public Vector getMax(Vector vec) {
		return roll(vec.setX(Xmax).setY(Ymax).setZ(Zmax));
	}

	public Location getMax(Location loc) {
		return loc.zero().add(roll(new Vector(Xmax, Ymax, Zmax)));
	}

	public Vector getMin(Vector vec) {
		return roll(vec.setX(Xmin).setY(Ymin).setZ(Zmin));
	}

	public Location getMin(Location loc) {
		return loc.zero().add(getMin(new Vector(Xmin, Ymin, Zmin)));
	}

	public List<Vector> getCorners() {
		List<Vector> set = new ArrayList<>();
		set.add(roll(new Vector(Xmin, Ymin, Zmin))); // 0,1 0,2 0,4
		set.add(roll(new Vector(Xmax, Ymin, Zmin))); // 1,3 1,5

		set.add(roll(new Vector(Xmin, Ymax, Zmin))); // 2,4
		set.add(roll(new Vector(Xmax, Ymax, Zmin))); //

		set.add(roll(new Vector(Xmin, Ymin, Zmax))); //
		set.add(roll(new Vector(Xmax, Ymin, Zmax))); // 5,3

		set.add(roll(new Vector(Xmin, Ymax, Zmax))); // 6,4 6,2
		set.add(roll(new Vector(Xmax, Ymax, Zmax))); // 7,6 7,5 7,3
		return set;
	}

	public Vector roll(Vector vec) {
		Collections.reverse(rank);
		rank.forEach(x -> x.accept(vec));
		Collections.reverse(rank);
		return vec;
	}

	public Location roll(Location loc) {
		return loc.zero().add(roll(loc.toVector()));
	}

	private boolean neko(Vector start, Vector vec, Vector loc) {
		return loc != null && (vec == null || start.distanceSquared(loc) < start.distanceSquared(vec));
	}

	private Vector getX(Vector vec1, Vector vec2, double length) {
		Vector v = get(vec1, vec2, length, vec1.getX(), vec2.getX());
		return v == null || v.getY() < Ymin || v.getY() > Ymax || v.getZ() < Zmin || v.getZ() > Zmax ? null : v;
	}

	private Vector getY(Vector vec1, Vector vec2, double length) {
		Vector v = get(vec1, vec2, length, vec1.getY(), vec2.getY());
		return v == null || v.getX() < Xmin || v.getX() > Xmax || v.getZ() < Zmin || v.getZ() > Zmax ? null : v;
	}

	private Vector getZ(Vector vec1, Vector vec2, double length) {
		Vector v = get(vec1, vec2, length, vec1.getZ(), vec2.getZ());
		return v == null || v.getX() < Xmin || v.getX() > Xmax || v.getY() < Ymin || v.getY() > Ymax ? null : v;
	}

	private Vector get(Vector v1, Vector v2, double line, double v1num, double v2num) {
		double num = v2num - v1num;
		if(num * num < 1.000000011686097E-007D)
			return null;
		double d = (line - v1num) / num;
		if(d < 0 || d > 1)
			return null;
		return new Vector(v1.getX() + (v2.getX() - v1.getX()) * d, v1.getY() + (v2.getY() - v1.getY()) * d, v1.getZ()
				+ (v2.getZ() - v1.getZ()) * d);
	}

	public void drawCorner(World world, Particle particle, int points) {
		List<Vector> corners = getCorners();
		corners.forEach(x -> world.spawnParticle(particle, x.toLocation(world).add(locX, locY, locZ), 1, 0, 0, 0, 0));
	}

	public void drawBox(World world, Particle particle, int points) {
		List<Vector> corners = getCorners();
		line(world, particle, corners.get(0), corners.get(1), points);
		line(world, particle, corners.get(0), corners.get(2), points);
		line(world, particle, corners.get(0), corners.get(4), points);

		line(world, particle, corners.get(1), corners.get(3), points);
		line(world, particle, corners.get(1), corners.get(5), points);

		line(world, particle, corners.get(2), corners.get(3), points);

		line(world, particle, corners.get(5), corners.get(4), points);

		line(world, particle, corners.get(6), corners.get(4), points);
		line(world, particle, corners.get(6), corners.get(2), points);

		line(world, particle, corners.get(7), corners.get(6), points);
		line(world, particle, corners.get(7), corners.get(5), points);
		line(world, particle, corners.get(7), corners.get(3), points);
	}

	private void line(World world, Particle p, Vector a, Vector b, int c) {
		Location from = a.toLocation(world).add(locX, locY, locZ);
		Vector move = b.clone().subtract(a).divide(new Vector(c, c, c));
		while (c-- >= 0)
			world.spawnParticle(p, from.add(move), 1, 0, 0, 0, 0);
	}

	public void drawCenter(World world, Particle particle) {
		Location center = getCenter(new Location(world, 0, 0, 0));
		Vector v = center.getDirection();
		world.spawnParticle(Particle.END_ROD, center, 0, v.getX(), v.getY(), v.getZ());
		world.spawnParticle(particle, center, 0, 0, 0, 0, 0);
	}

	public void drawRollPos(World world, Particle particle) {
		world.spawnParticle(particle, getCenter(new Location(world, 0, 0, 0)).add(roll(new Vector(0, 1, 0))), 0, 0, 0, 0, 0);
	}

	public double length() {
		return Math.sqrt((Xmax - Xmin) * (Xmax - Xmin) + (Ymax - Ymin) * (Ymax - Ymin) + (Zmax - Zmin) * (Zmax - Zmin));
	}

	public class HitResult {
		private final boolean	isHit;
		private final BlockFace	face;
		private final Vector	hitpos;

		private HitResult(BlockFace face, Vector vec) {
			this.face = face;
			this.hitpos = vec;
			this.isHit = vec != null;
		}

		public boolean isHit() {
			return isHit;
		}

		public BlockFace getDirection() {
			return face;
		}

		public Location getBoxPosition() {
			return isHit ? hitpos.clone().toLocation(world) : null;
		}

		public Location getWorldPosition() {
			return isHit ? roll(hitpos.clone()).toLocation(world).add(locX, locY, locZ) : null;
		}

		@Override
		public String toString() {
			return !isHit ? "HitResult:{hit = " + isHit + "}" : "HitResult:{hit = " + isHit + ", facing = " + face
					+ ", position = (X=" + hitpos.getX() + ", Y="
					+ hitpos.getY() + ", Z=" + hitpos.getZ() + ")}";
		}
	}

	public enum Rank {
		YAW, PITCH, ROLL
	}
}