package kame.api.collision;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import kame.api.baseapi.Vx_x_Rx;
import kame.api.collision.CollisionPosition;

public class Collider {

	private Collider() {}

	public static CollisionPosition rayTrace(Location start, Location end) {
		return rayTrace(start, end, false, false, false);
	}

	public static CollisionPosition rayTrace(Location start, Location end, boolean liquid) {
		return rayTrace(start, end, liquid, false, false);
	}

	public static CollisionPosition rayTrace(Location start, Location end, boolean liquid, boolean pathUncollidable, boolean returnuncollidable) {
		return Vx_x_Rx.get().rayTrace(start, end, liquid, pathUncollidable, returnuncollidable);
	}

	public static CollisionPosition rayTrace(Location start, Location end, Vector box1, Vector box2) {
		return Vx_x_Rx.get().rayTrace(start, end, box1, box2);
	}

	public static CollisionPosition rayTraceEntity(Location start, Location end, Collection<Entity> entities) {
		return Vx_x_Rx.get().rayTraceEntity(start, end, entities);
	}

	/**
	 * ワールドの地形とエンティティからはじめに衝突した部分を返すメソッド
	 * @param start 入射点
	 * @param end 出射点
	 * @param entities 除外する生き物
	 * @param liquid 液体を衝突判定に入れるか
	 * @param pathUncollidable 通り抜けられるブロックを通すか
	 * @param returnuncollidable 衝突がなかった場合endに指定した座標の点を返すか
	 * @return nullは戻ってこにゃい
	 */
	@Nonnull
	public static CollisionPosition rayTraceWorld(Location start, Location end, Collection<Entity> entities, boolean liquid, boolean pathUncollidable, boolean returnuncollidable) {
		return Vx_x_Rx.get().rayTraceWorld(start, end, entities, liquid, pathUncollidable, returnuncollidable);
	}

	@Nonnull
	public static List<CollisionPosition> rayTraceWorld(Location start, Location end, Collection<Entity> entities, boolean liquid, boolean pathUncollidable, boolean returnuncollidable, Class<? extends Entity> classfilter) {
		return Vx_x_Rx.get().rayTraceWorld(start, end, entities, liquid, pathUncollidable, returnuncollidable, classfilter);
	}

}
