package kame.api.collision;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;

public class CollisionPosition {

	private Object		collider;
	private BlockFace	face;
	private Location	loc;
	private boolean		block;
	private boolean		entity;
	private boolean		hit	= true;

	public CollisionPosition() {
		this.hit = false;
	}

	public CollisionPosition(BlockFace face, Location loc) {
		this.face = face;
		this.loc = loc;
	}

	public CollisionPosition(Block block, BlockFace face, Location loc) {
		this.collider = block;
		this.block = true;
		this.face = face;
		this.loc = loc;
	}

	public CollisionPosition(Entity entity, BlockFace face, Location loc) {
		this.collider = entity;
		this.entity = true;
		this.face = face;
		this.loc = loc;
	}

	public boolean isHit() {
		return hit;
	}

	public boolean isBlock() {
		return hit && block;
	}

	public boolean isEntity() {
		return hit && entity;
	}

	public boolean isBox() {
		return hit && !entity && !block;
	}

	@SuppressWarnings("unchecked")
	@Deprecated
	public <T> T getObject() {
		return (T) collider;
	}

	public <T> T getObject(Class<T> classFilter) {
		return classFilter.isInstance(collider) ? classFilter.cast(collider) : null;
	}

	public Block getBlock() {
		return block ? (Block) collider : null;
	}

	public Entity getEntity() {
		return block ? null : (Entity) collider;
	}

	public BlockFace getDirection() {
		return face;
	}

	public Location getLocation() {
		return loc == null ? null : loc.clone();
	}

	@Override
	public String toString() {
		return "pos:[" + (loc != null ? loc.toVector() : null) + "], tile = " + face + ", Object = " + collider;
	}
}
