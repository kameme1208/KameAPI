package kame.api.collision.ray;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import com.google.common.collect.Sets;

import kame.kameplayer.baseutils.ray.RayUtils;

public class BlockRay extends Ray {

	public final Set<RayHit> results = new TreeSet<>(Ray.comparator);
	public final static Set<Material> materials = Sets.newHashSet(Material.AIR, Material.WATER, Material.STATIONARY_WATER, Material.LAVA, Material.STATIONARY_LAVA);

	private static final Predicate<Block> Air = x -> x.isEmpty();
	private static final Predicate<Block> Liquid = x -> x.isLiquid();

	BlockRay(Location start, Location end, Map<Predicate<Block>, FilterType> filters) {
		RayUtils.rayTraceBlock(results, start.getWorld(), start.toVector(), end.toVector(), filters);
	}

	/**
	 * 直線上にいるブロックを取得するクラスのBuilderをLocationの座標と向きから生成します。
	 * @param loc - 座標と向きが設定されているLocationインスタンス
	 * @return - 生成されたBuilderクラス
	 */
	public static BlockRayBuilder from(Location loc) {
		return new BlockRayBuilders(loc);
	}

	/**
	 * 直線上にいるブロックを取得するクラスのBuilderをLivingEntityの座標と向きから生成します。
	 * @param entity - LivingEntityインスタンス
	 * @return - 生成されたBuilderクラス
	 */
	public static BlockRayBuilder from(LivingEntity entity) {
		return from(entity.getEyeLocation());
	}

	/**
	 * 直線上にいるブロックを取得するクラスのBuilderを2つのLocationから生成します。
	 * <br>この生成方法は終点の位置に誤差が発生する可能性があります。
	 * @param from - 始点の座標
	 * @param to - 終点の座標
	 * @return - 生成されたBuilderクラス
	 */
	public static BlockRayBuilder from(Location from, Location to) {
		Location clone = from.clone().setDirection(from.toVector().subtract(to.toVector()));
		return from(clone).setDistance(from.distance(to));
	}

	public static final Predicate<Block> Air() {
		return Air;
	}

	public static final Predicate<Block> AirOrLiquid() {
		return Air.or(Liquid);
	}
	
	public interface BlockRayBuilder extends RayBuilder {
		public BlockRayBuilder useTemplateFilter();
		public BlockRayBuilder setDistance(double distance);

		/**
		 * PredicateがTrueを返すと、このオブジェクトを判定結果から除外するフィルターを追加します。
		 * @param filter - 判定される関数
		 * @return - 自身のインスタンス
		 */
		public BlockRayBuilder skipBlock(Predicate<Block> filter);
		/**
		 * PredicateがTrueを返すと、このオブジェクトを判定結果に含むフィルターを追加します
		 * @param filter - 判定される関数
		 * @return - 自身のインスタンス
		 */
		public BlockRayBuilder includeBlock(Predicate<Block> filter)
		/**
		 * PredicateがTrueを返すとそこでチェックを終了するフィルターを追加します
		 * @param filter - 判定される関数
		 * @return - 自身のインスタンス
		 */;
		public BlockRayBuilder stopBlock(Predicate<Block> filter);
		
		public Ray trace();
	}

	public static class BlockRayBuilders implements BlockRayBuilder {
		private Location start;
		private double disntace = 256;
		private Map<Predicate<Block>, FilterType> block = new LinkedHashMap<>();
		private BlockRayBuilders(Location loc) {
			this.start = loc;
		}

		@Override
		public BlockRayBuilder setDistance(double distance) {
			this.disntace = distance;
			return this;
		}

		@Override
		public BlockRay trace() {
			return new BlockRay(start, start.clone().add(start.getDirection().multiply(disntace)), block);
		}

		/**
		 * 標準設定のフィルターを追加適用するメソッドです
		 * フィルター内容: 空気ブロック以外と衝突したときに衝突したブロックを返す
		 * @return - 自身のインスタンス
		 */
		public BlockRayBuilder useTemplateFilter() {
			block.put(x -> x.isEmpty(), FilterType.SKIP);
			return this;
		}

		/**
		 * 線上にいるBlockの判定をするフィルターを追加するメソッドです
		 * @param filter - 判定される関数
		 * @param type - フィルターの種類
		 * @return - 自身のインスタンス
		 */
		public BlockRayBuilder addBlockFilter(Predicate<Block> filter, FilterType type) {
			this.block.put(filter, type);
			return this;
		}
		
		public BlockRayBuilder skipBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.SKIP);
			return this;
		}
		
		public BlockRayBuilder includeBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.INCLUDE);
			return this;
		}
		
		public BlockRayBuilder stopBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.STOP);
			return this;
		}
	}

	public static class BlockRayHit extends RayHit {
		private final Block block;
		private final BlockFace face;

		public BlockRayHit(Vector from, Vector hitloc, Block block, BlockFace face) {
			super(from, hitloc);
			this.block = block;
			this.face = face;
		}

		/**
		 * 衝突したブロックを返します。
		 * @return - ブロック
		 */
		public Block getBlock() {
			return block;
		}

		@Override
		public Location getLocation() {
			return hitloc.toLocation(block.getWorld());
		}

		@Override
		public BlockFace getDirection() {
			return face;
		}

		@Override
		public HitType getType() {
			return HitType.BLOCK;
		}

		@Override
		public String toString() {
			return "BlockRayHit:{Pos:{x=" + (float)hitloc.getX() + ", y=" + (float)hitloc.getY() + ", z=" + (float)hitloc.getZ() + "}, Block=" + block.getType() + ", Face=" + face + "}";
		}
	}
	@Override
	public boolean isEmpty() {
		return results.isEmpty();
	}

	@Override
	public Iterator<RayHit> iterator() {
		return results.iterator();
	}

}
