package kame.api.collision.ray;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import kame.kameplayer.baseutils.ray.RayUtils;

public class EntityRay extends Ray {

	public final TreeSet<RayHit> results = new TreeSet<>(Ray.comparator);
	private static final Predicate<Entity> LivingEntity = x -> x instanceof LivingEntity;

	EntityRay(Location start, Location end, Map<Predicate<Entity>, FilterType> filters) {
		RayUtils.rayTraceEntity(results, start.getWorld(), start.toVector(), end.toVector(), filters);
	}

	/**
	 * 直線上にいるエンティティを取得するクラスのBuilderをLocationの座標と向きから生成します。
	 * @param loc - 座標と向きが設定されているLocationインスタンス
	 * @return - 生成されたBuilderクラス
	 */
	public static EntityRayBuilder from(Location loc) {
		return new EntityRayBuilders(loc);
	}

	/**
	 * 直線上にいるエンティティを取得するクラスのBuilderをLivingEntityの座標と向きから生成します。
	 * @param entity - LivingEntityインスタンス
	 * @return - 生成されたBuilderクラス
	 */
	public static EntityRayBuilder from(LivingEntity entity) {
		return new EntityRayBuilders(entity);
	}

	/**
	 * 直線上にいるエンティティを取得するクラスのBuilderを2つのLocationから生成します。
	 * <br>この生成方法は終点の位置に誤差が発生する可能性があります。
	 * @param from - 始点の座標
	 * @param to - 終点の座標
	 * @return - 生成されたBuilderクラス
	 */
	public static EntityRayBuilder from(Location from, Location to) {
		Location clone = from.clone().setDirection(from.toVector().subtract(to.toVector()));
		return from(clone).setDistance(from.distance(to));
	}

	public static Predicate<Entity> LivingEntity() {
		return LivingEntity;
	}
	
	public interface EntityRayBuilder extends RayBuilder {
		public EntityRayBuilder useTemplateFilter();
		public EntityRayBuilder setDistance(double distance);

		/**
		 * PredicateがTrueを返すと、このオブジェクトを判定結果から除外するフィルターを追加します。
		 * @param filter - 判定される関数
		 * @return - 自身のインスタンス
		 */
		public EntityRayBuilder skipEntity(Predicate<Entity> filter);
		/**
		 * PredicateがTrueを返すと、このオブジェクトを判定結果に含むフィルターを追加します
		 * @param filter - 判定される関数
		 * @return - 自身のインスタンス
		 */
		public EntityRayBuilder includeEntity(Predicate<Entity> filter)
		/**
		 * PredicateがTrueを返すとそこでチェックを終了するフィルターを追加します
		 * @param filter - 判定される関数
		 * @return - 自身のインスタンス
		 */;
		public EntityRayBuilder stopEntity(Predicate<Entity> filter);
		
		public Ray trace();
	}

	public static class EntityRayBuilders implements EntityRayBuilder {
		private Location start;
		private double disntace = 256;
		private LivingEntity e;
		private Map<Predicate<Entity>, FilterType> entity = new LinkedHashMap<>();
		private EntityRayBuilders(Location loc) {
			this.start = loc;
		}

		private EntityRayBuilders(LivingEntity entity) {
			this.start = entity.getEyeLocation();
			this.e = entity;
		}

		@Override
		public EntityRayBuilder setDistance(double distance) {
			this.disntace = distance;
			return this;
		}

		@Override
		public EntityRay trace() {
			return new EntityRay(start, start.clone().add(start.getDirection().multiply(disntace)), entity);
		}

		/**
		 * 標準設定のフィルターを追加適用するメソッドです
		 * フィルター内容:宣言で渡された生物(存在するなら)以外と衝突したときに衝突したエンティティを返す
		 * @return - 自身のインスタンス
		 */
		public EntityRayBuilder useTemplateFilter() {
			if(e != null)entity.put(x -> x == e, FilterType.SKIP);
			return this;
		}

		/**
		 * 線上にいるEntityの判定をするフィルターを追加するメソッドです
		 * @param filter - 判定される関数
		 * @param type - フィルターの種類
		 * @return - 自身のインスタンス
		 */
		public EntityRayBuilder addEntityFilter(Predicate<Entity> filter, FilterType type) {
			this.entity.put(filter, type);
			return this;
		}
		
		public EntityRayBuilder skipEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.SKIP);
			return this;
		}
		
		public EntityRayBuilder includeEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.INCLUDE);
			return this;
		}
		
		public EntityRayBuilder stopEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.STOP);
			return this;
		}
	}

	public static class EntityRayHit extends RayHit {
		private final Entity entity;
		private final BlockFace face;

		public EntityRayHit(Vector from, Vector hitloc, Entity block, BlockFace face) {
			super(from, hitloc);
			this.entity = block;
			this.face = face;
		}

		/**
		 * 衝突したエンティティを返します。
		 * @return - エンティティ
		 */
		public Entity getEntity() {
			return entity;
		}

		@Override
		public Location getLocation() {
			return hitloc.toLocation(entity.getWorld());
		}

		@Override
		public BlockFace getDirection() {
			return face;
		}

		@Override
		public HitType getType() {
			return HitType.ENTITY;
		}

		@Override
		public String toString() {
			return "EntityRayHit:{Pos:{x=" + (float)hitloc.getX() + ", y=" + (float)hitloc.getY() + ", z=" + (float)hitloc.getZ() + "}, Entity=" + entity.getType() + ", Face=" + face + "}";
		}
	}

	@Override
	public boolean isEmpty() {
		return results.isEmpty();
	}

	@Override
	public Iterator<RayHit> iterator() {
		return results.iterator();
	}

}
