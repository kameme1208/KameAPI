package kame.api.collision.ray;

import java.util.Comparator;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import kame.api.collision.ray.Ray.RayHit;


/**
 * このクラスは衝突判定の最低限の機能を提供する抽象クラスです。
 * @author kameme
 */
public abstract class Ray implements Iterable<RayHit> {

	protected static final Comparator<RayHit> comparator = (x, y) -> Double.compare(x.from.distanceSquared(x.hitloc), y.from.distanceSquared(y.hitloc));

	/**
	 * このインターフェースは衝突判定のクラスを作る最低限の機能を提供するインターフェースです
	 * @author kameme
	 */
	protected interface RayBuilder {
		/**
		 * 標準設定のフィルターを追加適用するメソッドです
		 * フィルター内容: 空気ブロック以外と衝突したときに衝突したブロックを返す
		 * @return - 自身のインスタンス
		 */
		public RayBuilder useTemplateFilter();
		
		/**
		 * 始点からの判定をする距離を決めます。
		 * @param distance - 距離
		 * @return - 自身のインスタンス
		 */
		public RayBuilder setDistance(double distance);

		/**
		 * 現在の地点での衝突判定を判定します。
		 * @return - 判定された結果を格納するクラス
		 */
		public Ray trace();
	}

	/**
	 * このクラスは衝突した部分の結果を示すための最低限の機能を提供する抽象クラスです。
	 * @author kameme
	 */
	public static abstract class RayHit {
		protected Vector from;
		protected Vector hitloc;

		protected RayHit(Vector from, Vector hitloc) {
			this.from = from;
			this.hitloc = hitloc;
		}

		/**
		 * このインスタンスの衝突した種類を返すメソッドです。
		 * @return - 衝突したものの種類(Entity, Block)
		 */
		public abstract HitType getType();

		/**
		 * 対象と衝突した位置を返すメソッドです。
		 * @return - 衝突した位置の正確な座標
		 */
		public abstract Location getLocation();

		/**
		 * 対象と衝突した面の向きを返すメソッドです。
		 * @return - 衝突した面
		 */
		public abstract BlockFace getDirection();

		public String toString() {
			return "Location:[x=" + hitloc.getX() + ", y=" + hitloc.getY() + ", z=" + hitloc.getZ() + "]";
		}
	}

	/**
	 * この結果に要素が1つも含まれていない場合にtrueを返します。
	 * @return - 1つでも何かに衝突した場合はtrue
	 */
	public abstract boolean isEmpty();

	/**
	 * RayHitインスタンスの衝突した種類を示すenum定数です
	 * @author kameme
	 */
	public enum HitType {
		/**
		 * EntityRayHitクラスである場合はこの定数が返されます。
		 */
		ENTITY,
		/**
		 * BlockRayHitクラスである場合はこの定数が返されます。
		 */
		BLOCK
	}

	/**
	 * フィルターの種類を示すenum定数です
	 * @author kameme
	 */
	public enum FilterType {
		/**
		 * フィルターの判定がtrueである時は判定した対象を無視します。
		 */
		SKIP,
		/**
		 * フィルターの判定がtrueである時は判定した対象を結果に含ませます。
		 */
		INCLUDE,
		/**
		 * フィルターの判定がtrueである時は判定した対象を結果に含ませ、これより後ろにある対象は無視し判定処理を中止します。
		 */
		STOP
	}

}
