package kame.api.collision.ray;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import kame.api.collision.ray.BlockRay.BlockRayBuilder;
import kame.api.collision.ray.BlockRay.BlockRayHit;
import kame.api.collision.ray.EntityRay.EntityRayBuilder;
import kame.api.collision.ray.EntityRay.EntityRayHit;
import kame.kameplayer.baseutils.ray.RayUtils;

public class WorldRay extends Ray {

	private final TreeSet<RayHit> results = new TreeSet<>(Ray.comparator);

	WorldRay(Location start, Location end, Map<Predicate<Block>, FilterType> block, Map<Predicate<Entity>, FilterType> entity) {
		RayUtils.rayTraceEntity(results, start.getWorld(), start.toVector(), end.toVector(), entity);
		RayUtils.rayTraceBlock (results, start.getWorld(), start.toVector(), end.toVector(), block);
		Iterator<RayHit> each = results.iterator();
		while (each.hasNext()) {
			RayHit hit = each.next();
			FilterType last = check(hit, block, entity);
			if(last == FilterType.STOP) {
				while(each.hasNext() && each.next() != null)each.remove();
			}else if(last == FilterType.SKIP) {
				each.remove();
			}
		}
	}

	protected FilterType check(RayHit hit, Map<Predicate<Block>, FilterType> block, Map<Predicate<Entity>, FilterType> entity) {
		if(hit instanceof BlockRayHit) {
			for(Entry<Predicate<Block>, FilterType> filter : block. entrySet())if(filter.getKey().test((((BlockRayHit) hit).getBlock())))return filter.getValue();
		}else if(hit instanceof EntityRayHit) {
			for(Entry<Predicate<Entity>, FilterType>filter : entity.entrySet())if(filter.getKey().test(((EntityRayHit) hit).getEntity()))return filter.getValue();
		}
		return FilterType.STOP;
	}

	/**
	 * 直線上にいるエンティティまたはブロックを取得するクラスのBuilderをLocationの座標と向きから生成します。
	 * @param loc - 座標と向きが設定されているLocationインスタンス
	 * @return - 生成されたBuilderクラス
	 */
	public static WorldRayBuilder from(Location loc) {
		return new WorldRayBuilder(loc);
	}

	/**
	 * 直線上にいるエンティティまたはブロックを取得するクラスのBuilderをLivingEntityの座標と向きから生成します。
	 * @param entity - LivingEntityインスタンス
	 * @return - 生成されたBuilderクラス
	 */
	public static WorldRayBuilder from(LivingEntity entity) {
		return new WorldRayBuilder(entity);
	}

	/**
	 * 直線上にいるエンティティまたはブロックを取得するクラスのBuilderを2つのLocationから生成します。
	 * <br>この生成方法は終点の位置に誤差が発生する可能性があります。
	 * @param from - 始点の座標
	 * @param to - 終点の座標
	 * @return - 生成されたBuilderクラス
	 */
	public static WorldRayBuilder from(Location from, Location to) {
		Location clone = from.clone().setDirection(from.toVector().subtract(to.toVector()));
		return from(clone).setDistance(from.distance(to));
	}

	public static class WorldRayBuilder implements BlockRayBuilder, EntityRayBuilder {
		private Location start;
		private double disntace = 256;
		private LivingEntity e;
		private Map<Predicate<Block> , FilterType> block  = new LinkedHashMap<>();
		private Map<Predicate<Entity>, FilterType> entity = new LinkedHashMap<>();
		private WorldRayBuilder(Location loc) {
			this.start = loc;
		}

		private WorldRayBuilder(LivingEntity entity) {
			this.start = entity.getEyeLocation();
			this.e = entity;
		}

		public WorldRayBuilder setDistance(double distance) {
			this.disntace = distance;
			return this;
		}

		/**
		 * 標準設定のフィルターを追加適用するメソッドです
		 * フィルター内容: 空気ブロックと、宣言で渡された生物(存在するなら)以外と衝突したときに衝突した判定を返す
		 * @return - 自身のインスタンス
		 */
		public WorldRayBuilder useTemplateFilter() {
			block.put(x -> x.isEmpty(), FilterType.SKIP);
			if(e != null)entity.put(x -> x == e, FilterType.SKIP);
			return this;
		}
		
		public WorldRayBuilder skipBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.SKIP);
			return this;
		}
		
		public WorldRayBuilder includeBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.INCLUDE);
			return this;
		}
		
		public WorldRayBuilder stopBlock(Predicate<Block> filter) {
			block.put(filter, FilterType.STOP);
			return this;
		}
		
		public WorldRayBuilder skipEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.SKIP);
			return this;
		}
		
		public WorldRayBuilder includeEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.INCLUDE);
			return this;
		}
		
		public WorldRayBuilder stopEntity(Predicate<Entity> filter) {
			entity.put(filter, FilterType.STOP);
			return this;
		}

		/**
		 * 線上にいるBlockの判定をするフィルターを追加するメソッドです
		 * @param filter - 判定される関数
		 * @param type - フィルターの種類
		 * @return - 自身のインスタンス
		 */
		public WorldRayBuilder addBlockFilter(Predicate<Block> filter, FilterType type) {
			block.put(filter, type);
			return this;
		}

		/**
		 * 線上にいるEntityの判定をするフィルターを追加するメソッドです
		 * @param filter - 判定される関数
		 * @param type - フィルターの種類
		 * @return - 自身のインスタンス
		 */
		public WorldRayBuilder addEntityFilter(Predicate<Entity> filter, FilterType type) {
			entity.put(filter, type);
			return this;
		}

		@Override
		public WorldRay trace() {
			return new WorldRay(start, start.clone().add(start.getDirection().multiply(disntace)), block, entity);
		}
	}

	@Override
	public boolean isEmpty() {
		return results.isEmpty();
	}

	@Override
	public Iterator<RayHit> iterator() {
		return results.iterator();
	}

}
