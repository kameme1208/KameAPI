package kame.api.data;

import kame.api.data.base.ListNode;
import kame.api.data.base.Node;
import kame.api.data.base.NumberNode;
import kame.api.data.base.SectionNode;
import kame.api.data.base.ValueNode;
import kame.api.data.value.BoolNode;
import kame.api.data.value.ByteNode;
import kame.api.data.value.DoubleNode;
import kame.api.data.value.FloatNode;
import kame.api.data.value.IntNode;
import kame.api.data.value.LongNode;
import kame.api.data.value.ShortNode;
import kame.api.data.value.StringNode;
import kame.kameplayer.baseutils.data.NodeType;

public abstract class Nodes extends NodeType<Node> {
	
	protected Nodes(Class<Node> b, Class<? extends Node> c) {super(b, c);}
	public static final NodeType<SectionNode> Section = NodeType.Section;
	public static final NodeType<ListNode   > List    = NodeType.List;
	public static final NodeType<StringNode > String  = NodeType.String;
	public static final NodeType<BoolNode   > Bool    = NodeType.Bool;
	public static final NodeType<ByteNode   > Byte    = NodeType.Byte;
	public static final NodeType<ShortNode  > Short   = NodeType.Short;
	public static final NodeType<IntNode    > Int     = NodeType.Int;
	public static final NodeType<LongNode   > Long    = NodeType.Long;
	public static final NodeType<FloatNode  > Float   = NodeType.Float;
	public static final NodeType<DoubleNode > Double  = NodeType.Double;
	public static final NodeType<ValueNode  > Value   = NodeType.Value;
	public static final NodeType<NumberNode > Number  = NodeType.Number;
	
	public static Node wrap(Node node) {
		return node != null ? node : Value.create();
	}
	
}
