package kame.api.data.base;

import java.util.List;

public interface ListNode extends Node, List<Node> {

	public ListNode or(ListNode def);
	public ListNode trash();
	
}
