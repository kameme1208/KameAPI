package kame.api.data.base;

import kame.kameplayer.baseutils.data.NodeType;

public interface Node {

	public boolean hasValue();

	public <T extends Node> T castTo(NodeType<T> type);
	
	public Node trash();
	
}