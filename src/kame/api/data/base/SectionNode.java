package kame.api.data.base;

import java.util.Map;

import kame.kameplayer.baseutils.data.NodeType;

public interface SectionNode extends Node, Map<String, Node> {
	public <T extends Node> T get(String key, NodeType<T> type);
	public Node set(String key, Node value);
	public SectionNode or(SectionNode def);
	public SectionNode trash();
}
