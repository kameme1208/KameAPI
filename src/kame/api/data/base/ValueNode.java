package kame.api.data.base;

public interface ValueNode extends Node {
	public interface IValueNode<V> extends Node {
		public V orElse(V def);
		public V orSet(V def);
		public <T extends Throwable> V orThrow(T thrown) throws T;
	}
	public ValueNode trash();
}
