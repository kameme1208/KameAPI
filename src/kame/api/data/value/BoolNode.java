package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface BoolNode extends ValueNode, IValueNode<Boolean> {
	public BoolNode or(BoolNode or);
	
	public BoolNode trash();

	public BoolNode setValue(boolean value);
}
