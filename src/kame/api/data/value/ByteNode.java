package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface ByteNode extends ValueNode, IValueNode<Byte> {
	public ByteNode or(ByteNode or);
	public ByteNode trash();

	public ByteNode setValue(byte value);
}
