package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface DoubleNode extends ValueNode, IValueNode<Double> {
	public DoubleNode or(DoubleNode or);
	
	public DoubleNode trash();

	public DoubleNode setValue(double value);
}
