package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface FloatNode extends ValueNode, IValueNode<Float> {
	public FloatNode or(FloatNode or);
	
	public FloatNode trash();

	public FloatNode setValue(float value);
}
