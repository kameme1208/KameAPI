package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface IntNode extends ValueNode, IValueNode<Integer> {
	public IntNode or(IntNode or);
	
	public IntNode trash();

	public IntNode setValue(int value);
}
