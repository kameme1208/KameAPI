package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface LongNode extends ValueNode, IValueNode<Long> {
	public LongNode or(LongNode or);
	
	public LongNode trash();

	public LongNode setValue(long value);
}
