package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface ShortNode extends ValueNode, IValueNode<Short> {
	public ShortNode or(ShortNode or);
	
	public ShortNode trash();

	public ShortNode setValue(short value);
}
