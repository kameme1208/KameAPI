package kame.api.data.value;

import kame.api.data.base.ValueNode;
import kame.api.data.base.ValueNode.IValueNode;

public interface StringNode extends ValueNode, IValueNode<String> {
	public StringNode or(StringNode or);
	
	public StringNode trash();

	public StringNode setValue(String value);
}
