package kame.api.inventory;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Sets;

import kame.api.inventory.ActionInventory;
import kame.api.inventory.ActionItemStack;
import kame.api.inventory.ActionItemStack.ClickEvent;
import kame.api.inventory.ActionItemStack.DragEvent;
import kame.kameplayer.Main;

public class ActionInventory implements Listener {

	private static final ActionInventory instance;

	public class ActionHolder implements InventoryHolder {
		private Player player;
		private ActionHolder(Player player) {this.player = player;}
		public Inventory getInventory() {
			return player.getOpenInventory().getTopInventory();
		}
	}

	private static Map<ActionHolder, ActionInventory> inventories = new HashMap<>();
	private static Map<Player, ActionHolder> players = new HashMap<>();

	static {
		Bukkit.getPluginManager().registerEvents(new ActionInventory(), Main.getPlugin());
		instance = new ActionInventory();
		Bukkit.getScheduler().runTaskTimer(Main.getPlugin(),() -> {
			players.entrySet().removeIf(x -> !x.getKey().isOnline());
			inventories.entrySet().removeIf(x -> !players.containsValue(x.getKey()));
		}, 200, 200);
	}

	private String title;
	private ItemStack[] items;
	private Set<Integer> glowing = new HashSet<>();

	private ActionInventory() {
		if(instance != null)throw new IllegalStateException("can not create this instance");
	}

	/**
	 * タイトルと容量を持つインベントリを作成します。
	 * @param title - インベントリのタイトル
	 * @param size - インベントリの容量(9の倍数である必要があります)
	 */
	public ActionInventory(String title, int size) {
		this.title = title;
		this.items = new ItemStack[size <= 9 ? 9 : size <= 18 ? 18 : size <= 27 ? 27 : size <= 36 ? 36 : size <= 45 ? 45 : size <= 54 ? 54 : 63];
	}

	/**
	 * インベントリのタイトルを設定します。
	 * @param title - 設定するタイトルの文字列
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 指定したインベントリのスロットにItemStackを設置します。
	 * @param index - インベントリのスロット
	 * @param item - 設置するItemStack
	 * @return
	 */
	public ActionInventory setItem(int index, ItemStack item) {
		items[index] = item;
		if(!item.getEnchantments().isEmpty())addGlow(index);
		return this;
	}

	/**
	 * 指定したインベントリのスロットにあるItemStackを取得します。
	 * @param index - インベントリのスロット
	 * @return
	 */
	public ItemStack getItem(int index) {
		return items[index];
	}

	/**
	 * インベントリのタイトルを取得します。
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 指定したインベントリのスロットにアイテム名や説明文を設定して設置します。
	 * @param index - インベントリのスロット
	 * @param item - 設置するItemStack
	 * @param name - ItemStackに設定するアイテム名
	 * @param lore - ItemStackに設定する説明文
	 * @return
	 */
	public ActionInventory setMetaItem(int index, ItemStack item, String name, String... lore) {
		items[index] = setNameLore(item, name, lore);
		return this;
	}

	/**
	 * 指定したプレイヤーにこのインベントリを開かせます
	 * @param player
	 * @return
	 */
	public InventoryView openInventory(Player player) {
		ActionHolder holder = new ActionHolder(player);
		Inventory inv = Bukkit.createInventory(holder, items.length, title);
		for(int i = 0; i < items.length; i++)if(items[i] != null)inv.setItem(i, setGlow(items[i], glowing.contains(i)));
		InventoryView view = player.openInventory(inv);
		inventories.put(holder, this);
		players.put(player, holder);
		return view;
	}

	private ItemStack setGlow(ItemStack item, boolean glow) {
		item = item.clone();
		if(glow) {
			item.addUnsafeEnchantment(Enchantment.LUCK, 0);
		}else {
			item.getEnchantments().keySet().forEach(item::removeEnchantment);
		}
		return item;
	}

	@EventHandler(priority=EventPriority.MONITOR)
	private void onInventoryClick(InventoryClickEvent event) {
		Inventory inv = event.getInventory();
		if(inv.getHolder() instanceof ActionHolder) {
			event.setCancelled(true);
			if(event.getInventory() != event.getClickedInventory())return;
			ItemStack item = inventories.get(inv.getHolder()).items[event.getSlot()];
			if(!ActionItemStack.class.isInstance(item))return;
			for(ClickEvent e : ((ActionItemStack) item).click) {
				try {e.click(event);}catch(Exception ex) {
					Bukkit.getLogger().log(Level.SEVERE, "イベントの呼び出しで例が発生しました。" + e, ex);
				}
			}
		}
	}

	@EventHandler(priority=EventPriority.MONITOR)
	private void onDrag(InventoryDragEvent event) {
		Inventory inv = event.getInventory();
		if(inv.getHolder() instanceof ActionHolder) {
			event.setCancelled(true);
			ItemStack item = event.getOldCursor();
			if(!ActionItemStack.class.isInstance(item))return;
			for(DragEvent e : ((ActionItemStack) item).drag) {
				try {e.drag(event);}catch(Exception ex) {
					Bukkit.getLogger().log(Level.SEVERE, "イベントの呼び出しで例が発生しました。" + e, ex);
				}
			}
		}
	}

	@EventHandler
	private void onClose(InventoryCloseEvent event) {
		inventories.remove(players.remove(event.getPlayer()));
	}

	@EventHandler
	private void onDisable(PluginDisableEvent event) {
		if(Main.getPlugin().equals(event.getPlugin()))players.keySet().forEach(Player::closeInventory);
	}

	/**
	 * 指定したスロットのアイテムにエンチャントを付与し光っているように見せます。
	 * @param slot 
	 */
	public void setGlow(Integer... slot) {
		glowing = Sets.newHashSet(slot);
	}

	/**
	 * 指定したスロットのアイテムにエンチャントを付与し光っているように見せます。
	 * @param slot 
	 */
	public void setGlow(Collection<Integer> slot) {
		glowing = Sets.newHashSet(slot);
	}

	/**
	 * 指定したスロットのアイテムにエンチャントを付与し光っているように見せるスロットを追加します。
	 * @param slot 
	 */
	public void addGlow(Integer... slot) {
		glowing.addAll(Sets.newHashSet(slot));
	}

	/**
	 * 指定したスロットのアイテムにエンチャントを付与し光っているように見せるスロットを追加します。
	 * @param slot 
	 */
	public void addGlow(Collection<Integer> slot) {
		glowing.addAll(slot);
	}

	private ItemStack setNameLore(ItemStack item, String name, String... lore) {
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(name);
		im.setLore(Arrays.asList(lore));
		item.setItemMeta(im);
		return item;
	}

}
