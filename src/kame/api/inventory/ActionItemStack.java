package kame.api.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * このクラスはクリック時/ドラッグ時に登録したアクションを実行することができるクラスです。
 * @author かめめ
 *
 */
public class ActionItemStack extends ItemStack {
	List<ClickEvent> click = new ArrayList<>();
	List<DragEvent> drag = new ArrayList<>();

	public ActionItemStack(Material material, int amount, short durability) {
		super(material, amount, durability);
	}

	public ActionItemStack(Material material, int amount) {
		super(material, amount);
	}

	public ActionItemStack(Material material) {
		super(material);
	}

	public ActionItemStack(ItemStack item) {
		super(item);
	}

	/**
	 * クリックしたときに実行されるイベントを追加します。
	 * <br><b>実装例:</b>
	 * <code>
	 * actionitem.addClickEvent(event -> { 実行処理 });
	 * </code>
	 *
	 * @param event - 実行される処理
	 */
	public void addClickEvent(ClickEvent event) {
		click.add(event);
	}


	/**
	 * ドラッグしたときに実行されるイベントを追加します。
	 * <br><b>実装例:</b>
	 * <code>
	 * actionitem.addDragEvent(event -> { 実行処理 });
	 * </code>
	 *
	 * @param event - 実行される処理
	 */
	public void addDragEvent(DragEvent event) {
		drag.add(event);
	}

	public void setGlow() {
		addUnsafeEnchantment(Enchantment.LUCK, 1);
	}

	public void setDisplayName(String name) {
		ItemMeta im = super.getItemMeta();
		im.setDisplayName(name);
		super.setItemMeta(im);
	}

	public void setLocalizedName(String name) {
		ItemMeta im = super.getItemMeta();
		try {
			im.setLocalizedName(name);
		}catch(Exception e) {
			Bukkit.getLogger().log(Level.SEVERE, e.getMessage(), e);
		}
		super.setItemMeta(im);
	}

	public void setLore(String... name) {
		ItemMeta im = super.getItemMeta();
		im.setLore(Arrays.asList(name));
		super.setItemMeta(im);
	}

	public void addLore(String... name) {
		ItemMeta im = super.getItemMeta();
		List<String> lore = im.hasLore() ? im.getLore() : new ArrayList<>();
		lore.addAll(Arrays.asList(name));
		im.setLore(lore);
		super.setItemMeta(im);
	}

	public void addLore(Collection<String> name) {
		ItemMeta im = super.getItemMeta();
		List<String> lore = im.hasLore() ? im.getLore() : new ArrayList<>();
		lore.addAll(name);
		im.setLore(lore);
		super.setItemMeta(im);
	}

	public void addItemFlags(ItemFlag... flags) {
		ItemMeta im = super.getItemMeta();
		im.addItemFlags(flags);
		super.setItemMeta(im);
	}

	public interface ClickEvent {
		public void click(InventoryClickEvent event);
	}

	public interface DragEvent {
		public void drag(InventoryDragEvent event);
	}

	@Override
	public String toString() {
		return "ActionItemStack{" + super.toString() + "}";
	}
}
