package kame.api.nbt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import kame.api.nbt.interfaces.TagBase;

public class TagBuffer extends TagSection {

	public TagSection sec;

	public TagBuffer() {

	}

	public TagBuffer(String tagName, TagSection section) {
		this.sec = section;
		section.set(tagName, this);
	}

	@Override
	public boolean hasTag(String tagName) {
		return map.containsKey(tagName);
	}

	@Override
	public boolean hasTag(String tagName, Class<?> classFilter) {
		return classFilter.isInstance(map.get(tagName));
	}

	@Override
	public Set<String> keySet() {
		return map.keySet();
	}

	@Override
	public Object get(String tagName) {
		return map.get(tagName);
	}

	@Override
	public <T> T get(String tagName, Class<T> classFilter, T defaultValue) {
		Object obj = map.get(tagName);
		return classFilter.isInstance(obj) ? classFilter.cast(obj) : defaultValue;
	}

	@Override
	public <T> List<T> list(String tagName, Class<T> classFilter, List<T> putList) {
		Object obj = map.get(tagName);
		if(obj instanceof List) {
			((List<?>) obj).forEach(x -> {if(classFilter.isInstance(x))putList.add(classFilter.cast(x));});
		}
		return putList;
	}

	@Override
	public TagBuffer getSection(String tagName, boolean returnEmpty) {
		Object obj = map.get(tagName);
		return obj instanceof TagSection ? (TagBuffer) obj : returnEmpty ? new TagBuffer() : null;
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Class<?> getType(String tagName) {
		Object obj = map.get(tagName);
		return obj != null ? obj.getClass() : null;
	}

	@Override
	public Map<String, Object> toMap() {
		return new HashMap<>(map);
	}

	@Override
	public TagBuffer createSection(String tagName) {
		TagBuffer sec = new TagBuffer();
		map.put(tagName, sec);
		return sec;
	}

	@Override
	public List<TagSection> createSectionList(String tagName) {
		List<TagSection> sec = new ArrayList<>();
		map.put(tagName, sec);
		return sec;
	}

	@Override
	public List<TagSection> getSectionList(String tagName, boolean returnEmpty) {
		Object obj = map.get(tagName);
		if(obj instanceof List) {
			@SuppressWarnings("unchecked")
			List<TagSection> buffer = (List<TagSection>) obj;
			return buffer;
		}
		return returnEmpty ? new ArrayList<>() : null;
	}

	@Override
	public TagSection set(String tagName, Object nbt) {
		map.put(tagName, nbt);
		return this;
	}

	@Override
	public TagSection setSection(TagBase nbt) {
		map = nbt.toMap();
		return this;
	}

	@Override
	public TagReader toReader() {
		return new TagReader(this);
	}

	@Override
	public Object remove(String tagName) {
		return map.remove(tagName);
	}

	@Override
	public void save() {
		if(sec != null)sec.save();
	}
}
