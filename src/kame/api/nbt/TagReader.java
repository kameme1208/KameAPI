package kame.api.nbt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import kame.api.nbt.interfaces.TagBase;
import kame.api.nbt.interfaces.TagReadable;
import kame.kameplayer.baseutils.nbt.NBTReader;

/**
 * このクラスはTagReadableの実装クラスです、このクラスはNBTタグの高速な読み込みをサポートします。
 * @author かめめ
 */
public class TagReader implements TagReadable {
	private NBTReader reader;
	TagReader(TagBase section) {
		this.reader = NBTReader.getInstance(section);
	}

	private TagReader() {
		this.reader = NBTReader.getEmptyReader();
	}

	private TagReader(NBTReader reader) {
		this.reader = reader;
	}

	/**
	 * NBTタグのJsonStringからTagReaderのインスタンスを作成します。
	 * @param nbttag - NBTタグの文字列
	 */
	public TagReader(String nbttag) {
		try {
			this.reader = NBTReader.parse(nbttag);
		}catch (Exception e) {
			throw new IllegalArgumentException("cant parse NBT:" + nbttag);
		}
	}

	public static TagReader parseNBT(String nbttag) {
		if(nbttag == null)return null;
		try {
			return new TagReader(nbttag);
		}catch (Exception e) {
			return null;
		}
	}

	@Deprecated
	public static TagReader getTagReader(ItemStack item) {
		return new TagReader(item);
	}

	/**
	 * ItemStackからTagReaderのインスタンスを作成します。
	 * @param item - NBTタグを読み込むItemStackのインスタンス
	 */
	public TagReader(ItemStack item) {
		this.reader = NBTReader.getInstance(item);
	}

	/**
	 * BlockStateからTagReaderのインスタンスを作成します。
	 * @param block - NBTタグを読み込むBlockStateのインスタンス
	 */
	public TagReader(BlockState block) {
		this.reader = NBTReader.getInstance(block);
	}

	/**
	 * EntityからTagReaderのインスタンスを作成します。
	 * @param entity - NBTタグを読み込むEntityのインスタンス
	 */
	public TagReader(Entity entity) {
		this.reader = NBTReader.getInstance(entity);
	}

	@Override
	public boolean isEmpty() {
		return reader.isEmpty();
	}

	@Override
	public boolean hasTag(String tagName) {
		return reader.hasTag(tagName);
	}

	@Override
	public boolean hasTag(String tagName, Class<?> classFilter) {
		return reader.hasTag(tagName, classFilter);
	}

	@Override
	public Set<String> keySet() {
		return reader.keySet();
	}

	@Override
	public int size() {
		return reader.size();
	}

	@Override
	public Class<?> getType(String tagName) {
		return reader.getType(tagName);
	}

	@Override
	public Object get(String tagName) {
		return reader.get(tagName);
	}

	@Override
	public <T> List<T> list(String tagName, Class<T> classFilter, @Nonnull List<T> putList) {
		return reader.list(tagName, classFilter, putList);
	}

	@Override
	public <T> T get(String tagName, Class<T> classFilter, T defaultValue) {
		return reader.get(tagName, classFilter, defaultValue);
	}

	@Override
	public TagReader getSection(String tagName, boolean returnEmpty) {
		return reader.hasTag(tagName, Map.class) ? new TagReader(reader.getSection(tagName)) : returnEmpty ? new TagReader() : null;
	}

	@Override
	public List<TagReader> getSectionList(String tagName, boolean returnEmpty) {
		List<NBTReader> list = reader.getSectionList(tagName);
		if(list != null) {
			List<TagReader> reader = new ArrayList<>();
			list.forEach(x -> reader.add(new TagReader(x)));
			return reader;
		}
		return returnEmpty ? new ArrayList<>() : null;
	}

	@Override
	public Map<String, Object> toMap() {
		return reader.toMap();
	}

	@Override
	public String toString() {
		return reader.toString();
	}
}
