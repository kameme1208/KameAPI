package kame.api.nbt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kame.api.nbt.interfaces.TagBase;
import kame.api.nbt.interfaces.TagReadable;
import kame.api.nbt.interfaces.TagWriteable;
import kame.kameplayer.Main;

/**
 * このクラスはTagReadable,TagWriteableの実装クラスです、このクラスはNBTタグの読み込み、書き込みをサポートします。
 * @author かめめ
 */
public class TagSection implements TagReadable, TagWriteable {
	private Object base;
	private Object nms;
	private static VersionLoader ver;
	static {
		if(ver == null) {
			switch(Main.VERSION) {
			case "1_10_R1":
				ver = new V1_10_R1();
				break;
			case "1_11_R1":
				ver = new V1_11_R1();
				break;
			case "1_12_R1":
				ver = new V1_12_R1();
				break;
			}
		}
	}
	protected Map<String, Object> map;

	TagSection() {
		this.map = new HashMap<>();
	}

	public TagSection(ItemStack item) {
		this.base = item;
		this.nms = ver.copyNMS(item);
		this.map = ver.itemNBTMap(nms);
		if(nms == null)throw new NullPointerException();
		if(map == null)this.map = new HashMap<>();
	}

	public TagSection(Entity entity) {
		this.base = entity;
		this.nms = ver.copyNMS(entity);
		this.map = ver.entityNBTMap(nms);
		if(nms == null)throw new NullPointerException();
		if(map == null)this.map = new HashMap<>();
	}

	public TagSection(BlockState block) {
		this.base = block;
		this.nms = ver.copyNMS(block);
		this.map = ver.tileNBTMap(nms);
		if(nms == null)throw new NullPointerException();
		if(map == null)this.map = new HashMap<>();
	}

	public TagSection(String nbttag) {
		this.map = new TagReader(nbttag).toMap();
	}

	protected TagSection(TagSection old, String tagName) {
		@SuppressWarnings("unchecked")
		Map<String, Object> m = (Map<String, Object>) old.map.get(tagName);
		if(m == null)throw new NullPointerException();
		this.map = m;
		this.base = old;
	}

	@Override
	public TagSection createSection(String tagName) {
		TagSection sec = new TagSection();
		sec.base = this;
		map.put(tagName, sec);
		return sec;
	}

	@Override
	public List<TagSection> createSectionList(String tagName) {
		List<TagSection> list = new ArrayList<>(0);
		map.put(tagName, list);
		return list;
	}

	@Override
	public TagSection getSection(String tagName, boolean createEmpty) {
		Object obj = map.get(tagName);
		return obj instanceof Map ? new TagSection(this, tagName) : createEmpty ? createSection(tagName) : null;
	}

	@Override
	public List<TagSection> getSectionList(String tagName, boolean createEmpty) {
		Object tag = map.get(tagName);
		if(tag instanceof List && ((List<?>) tag).stream().allMatch(Map.class::isInstance)) {
			@SuppressWarnings("unchecked")
			List<TagSection> list = (List<TagSection>) tag;
			return list;
		}
		return createEmpty ? createSectionList(tagName) : null;
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean hasTag(String tagName) {
		return map.containsKey(tagName);
	}

	@Override
	public boolean hasTag(String tagName, Class<?> classFilter) {
		Object obj = map.get(tagName);
		return obj != null && classFilter.isInstance(obj);
	}

	@Override
	public Set<String> keySet() {
		return map.keySet();
	}

	@Override
	public Class<?> getType(String tagName) {
		Object obj = map.get(tagName);
		if(obj instanceof Boolean) {
			return boolean.class;
		}else if(obj instanceof Byte) {
			return byte.class;
		}else if(obj instanceof Short) {
			return short.class;
		}else if(obj instanceof Integer) {
			return int.class;
		}else if(obj instanceof Long) {
			return long.class;
		}else if(obj instanceof Float) {
			return  float.class;
		}else if(obj instanceof Double) {
			return double.class;
		}else if(obj instanceof byte[] || obj instanceof Byte[]) {
			return byte[].class;
		}else if(obj instanceof int[] || obj instanceof Integer[]) {
			return int[].class;
		}else if(obj instanceof String) {
			return String.class;
		}else if(obj instanceof List) {
			return List.class;
		}else if(obj instanceof Map) {
			return Map.class;
		}else if(obj instanceof TagBase) {
			return Map.class;
		}else {
			return obj != null ? obj.getClass() : null;
		}
	}

	@Override
	public Object get(String tagName) {
		return map.get(tagName);
	}

	@Override
	public <T> T get(String key, Class<T> classFilter, T defaultValue) {
		Object obj = map.get(key);
		return classFilter.isInstance(obj) ? classFilter.cast(obj) : defaultValue;
	}

	@Override
	public <T> List<T> list(String tagName, Class<T> classFilter, @Nonnull List<T> putList) {
		Object obj = map.get(tagName);
		if(obj instanceof List) {
			((List<?>) obj).forEach(x -> {if(classFilter.isInstance(x))putList.add(classFilter.cast(x));});
		}
		return putList;
	}

	@Override
	public TagSection set(String tagName, Object nbt) {
		checkData(nbt);
		map.put(tagName, nbt);
		return this;
	}

	@Override
	public TagSection setSection(TagBase section) {
		if(section instanceof TagSection) {
			this.map = ((TagSection) section).map;
		}else {
			this.map = section.toMap();
		}
		return this;
	}

	@Override
	public Map<String, Object> toMap() {
		return new HashMap<>(map);
	}

	/**
	 * このインスタンスの値をTagReaderにコピーした値を返します。
	 * @return このインスタンスをコピーしたTagReaderのインスタンス
	 */
	public TagReader toReader() {
		return new TagReader(this);
	}

	@Override
	public Object remove(String tagName) {
		return map.remove(tagName);
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public String toString() {
		Object str = TagTool.setNBTTag(map);
		return str != null ? str.toString() : "null";
	}

	/**
	 * 現在の値を変更元のインスタンス(ItemStack,Entity,BlockStateなど)に変更を適用させます。<br>このメソッドは変更元のインスタンスが同じであればどの階層から行っても全ての変更が適用されます。
	 * つまり<br><b>変更前</b>{Pets:{A:{type:"cat",name:"鳥"},B:{type:"dog",name:"虫"}}} (A,Bの両方を変更しBの階層でを実行)
	 * <br><b>変更後</b>{Pets:{A:{type:"cat",name:"猫"},B:{type:"dog",name:"犬"}}}
	 * <br>のように、どこのTagSectionでsaveを実行しても全ての変更が保存されます。
	 */
	public void save() {
		if(base instanceof TagSection) {
			((TagSection) base).save();
		}else if(base instanceof ItemStack) {
			((ItemStack) base).setItemMeta(ver.getItemMeta(nms, this));
		}else if(base instanceof Entity) {
			ver.saveEntity(nms, this);
		}else if(base instanceof BlockState) {
			ver.saveBlock(nms, this);
		}

	}

	private Object checkData(Object tag) {
		if(tag instanceof Boolean || tag instanceof Byte || tag instanceof Short || tag instanceof Integer || tag instanceof Long
				|| tag instanceof Float || tag instanceof Double || tag instanceof byte[] || tag instanceof Byte[] || tag instanceof int[]
				|| tag instanceof Integer[] || tag instanceof String || tag instanceof TagBase) {
			return tag;
		}else if(tag instanceof List){
			return new ArrayList<Object>((List<?>)tag);
		}else if(tag instanceof Map) {
			return new HashMap<Object, Object>((Map<?, ?>)tag);
		}else {
			throw new ClassCastException();
		}

	}

	@Deprecated
	public static TagSection getEmptySection() {
		return new TagSection();
	}

	@Deprecated
	public static TagSection getSection(ItemStack item) {
		return new TagSection(item);
	}

	private static interface VersionLoader {

		public Object copyNMS(ItemStack item);
		public Object copyNMS(Entity entity);
		public Object copyNMS(BlockState block);

		public Map<String, Object> itemNBTMap(Object nms);
		public Map<String, Object> entityNBTMap(Object nms);
		public Map<String, Object> tileNBTMap(Object nms);

		public void saveEntity(Object nms, TagSection sec);
		public void saveBlock(Object nms, TagSection sec);
		public ItemMeta getItemMeta(Object nmsitem, TagSection sec);
	}

	@SuppressWarnings("unchecked")
	private static class V1_10_R1 implements VersionLoader {

		private V1_10_R1() {
			if(ver != null)throw new IllegalStateException();
		}

		@Override
		public Object copyNMS(ItemStack item) {
			return org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack.asNMSCopy(item);
		}

		@Override
		public Object copyNMS(Entity entity) {
			return ((org.bukkit.craftbukkit.v1_10_R1.entity.CraftEntity)entity).getHandle();
		}

		@Override
		public Object copyNMS(BlockState block) {
			return ((org.bukkit.craftbukkit.v1_10_R1.CraftWorld)block.getWorld()).getHandle().getTileEntity(new net.minecraft.server.v1_10_R1.BlockPosition(block.getX(), block.getY(), block.getZ()));
		}

		@Override
		public Map<String, Object> itemNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag(((net.minecraft.server.v1_10_R1.ItemStack)nms).getTag());
		}

		@Override
		public Map<String, Object> entityNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag((((net.minecraft.server.v1_10_R1.Entity)nms).e(new net.minecraft.server.v1_10_R1.NBTTagCompound())));
		}

		@Override
		public Map<String, Object> tileNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag(((net.minecraft.server.v1_10_R1.TileEntity)nms).save(new net.minecraft.server.v1_10_R1.NBTTagCompound()));
		}

		@Override
		public void saveEntity(Object nms, TagSection sec) {
			((net.minecraft.server.v1_10_R1.Entity)nms).f(((net.minecraft.server.v1_10_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
		}

		@Override
		public void saveBlock(Object nms, TagSection sec) {
			((net.minecraft.server.v1_10_R1.TileEntity)nms).a(((net.minecraft.server.v1_10_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
			((net.minecraft.server.v1_10_R1.TileEntity)nms).update();
		}

		@Override
		public ItemMeta getItemMeta(Object nmsitem, TagSection sec) {
			((net.minecraft.server.v1_10_R1.ItemStack)nmsitem).setTag(((net.minecraft.server.v1_10_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
			return org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack.asCraftMirror((net.minecraft.server.v1_10_R1.ItemStack)nmsitem).getItemMeta();
		}
	}

	@SuppressWarnings("unchecked")
	private static class V1_11_R1 implements VersionLoader {

		private V1_11_R1() {
			if(ver != null)throw new IllegalStateException();
		}

		@Override
		public Object copyNMS(ItemStack item) {
			return org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack.asNMSCopy(item);
		}

		@Override
		public Object copyNMS(Entity entity) {
			return ((org.bukkit.craftbukkit.v1_11_R1.entity.CraftEntity)entity).getHandle();
		}

		@Override
		public Object copyNMS(BlockState block) {
			return ((org.bukkit.craftbukkit.v1_11_R1.CraftWorld)block.getWorld()).getHandle().getTileEntity(new net.minecraft.server.v1_11_R1.BlockPosition(block.getX(), block.getY(), block.getZ()));
		}

		@Override
		public Map<String, Object> itemNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag(((net.minecraft.server.v1_11_R1.ItemStack)nms).getTag());
		}

		@Override
		public Map<String, Object> entityNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag((((net.minecraft.server.v1_11_R1.Entity)nms).e(new net.minecraft.server.v1_11_R1.NBTTagCompound())));
		}

		@Override
		public Map<String, Object> tileNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag(((net.minecraft.server.v1_11_R1.TileEntity)nms).save(new net.minecraft.server.v1_11_R1.NBTTagCompound()));
		}

		@Override
		public void saveEntity(Object nms, TagSection sec) {
			((net.minecraft.server.v1_11_R1.Entity)nms).f(((net.minecraft.server.v1_11_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
		}

		@Override
		public void saveBlock(Object nms, TagSection sec) {
			((net.minecraft.server.v1_11_R1.TileEntity)nms).a(((net.minecraft.server.v1_11_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
			((net.minecraft.server.v1_11_R1.TileEntity)nms).update();
		}

		@Override
		public ItemMeta getItemMeta(Object nmsitem, TagSection sec) {
			((net.minecraft.server.v1_11_R1.ItemStack)nmsitem).setTag(((net.minecraft.server.v1_11_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
			return org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack.asCraftMirror((net.minecraft.server.v1_11_R1.ItemStack)nmsitem).getItemMeta();
		}
	}

	@SuppressWarnings("unchecked")
	private static class V1_12_R1 implements VersionLoader {

		private V1_12_R1() {
			if(ver != null)throw new IllegalStateException();
		}

		@Override
		public Object copyNMS(ItemStack item) {
			return org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack.asNMSCopy(item);
		}

		@Override
		public Object copyNMS(Entity entity) {
			return ((org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity)entity).getHandle();
		}

		@Override
		public Object copyNMS(BlockState block) {
			return ((org.bukkit.craftbukkit.v1_12_R1.CraftWorld)block.getWorld()).getHandle().getTileEntity(new net.minecraft.server.v1_12_R1.BlockPosition(block.getX(), block.getY(), block.getZ()));
		}

		@Override
		public Map<String, Object> itemNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag(((net.minecraft.server.v1_12_R1.ItemStack)nms).getTag());
		}

		@Override
		public Map<String, Object> entityNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag((((net.minecraft.server.v1_12_R1.Entity)nms).save(new net.minecraft.server.v1_12_R1.NBTTagCompound())));
		}

		@Override
		public Map<String, Object> tileNBTMap(Object nms) {
			return (Map<String, Object>)TagTool.getNBTTag(((net.minecraft.server.v1_12_R1.TileEntity)nms).save(new net.minecraft.server.v1_12_R1.NBTTagCompound()));
		}

		@Override
		public void saveEntity(Object nms, TagSection sec) {
			((net.minecraft.server.v1_12_R1.Entity)nms).f(((net.minecraft.server.v1_12_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
		}

		@Override
		public void saveBlock(Object nms, TagSection sec) {
			((net.minecraft.server.v1_12_R1.TileEntity)nms).a(((net.minecraft.server.v1_12_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
			((net.minecraft.server.v1_12_R1.TileEntity)nms).update();
		}

		@Override
		public ItemMeta getItemMeta(Object nmsitem, TagSection sec) {
			((net.minecraft.server.v1_12_R1.ItemStack)nmsitem).setTag(((net.minecraft.server.v1_12_R1.NBTTagCompound)TagTool.setNBTTag(sec)));
			return org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack.asCraftMirror((net.minecraft.server.v1_12_R1.ItemStack)nmsitem).getItemMeta();
		}
	}

}
