package kame.api.nbt;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import kame.api.baseapi.Vx_x_Rx;
import kame.api.nbt.interfaces.TagBase;
import kame.api.nbt.interfaces.TagReadable;

@SuppressWarnings("deprecation")
public class TagTool {

	public static class TagItem {

		/**
		 * 与えられたTagBaseからItemStackを作成します。
		 * このTagBaseからはNBTタグのルールに従ってItemStackが作成されます。
		 * つまりこのTagBase内の要素には<br>
		 * <code><br>
		 * {id:"minecraft:item",Count:1b,Damage:0s,tag:{}}</code>のように<br>
		 * <b>id</b> - アイテムの種類(String)<br>
		 * <b>Count</b> - アイテムの個数Byte)<br>
		 * <b>Damage</b> - アイテムの耐久値/メタ値(Short)<br>
		 * <b>nbt</b> - アイテムに付与されるNBT(Map)<br>
		 * @param section - ItemStackが作成されるNBTの階層
		 * @return NBTによって作成されたItemStack
		 */
		public static ItemStack createItemStack(TagReadable section) {
			if(section == null) return new ItemStack(Material.AIR);
			Material mate = Bukkit.getUnsafe().getMaterialFromInternalName(section.get("id", String.class, "material:air"));
			int amount = section.get("Count", Number.class, 0).intValue();
			short durability = section.get("Damage", Number.class, 0).shortValue();
			ItemStack item = new ItemStack(mate, amount, durability);
			if(item.getType() == Material.AIR) return null;
			if(section.hasTag("nbt", Map.class)) setAllMap(item, section.getSection("nbt", false).toMap());
			return item;
		}

		/**
		 * 与えられたTagBaseからItemStackの配列を作成します。
		 * このTagBaseからはcreateItemStackが参照するキーとアイテムの配列のインデックスを示すSlotからItemStackの配列が作成されます。
		 * つまりTagBaseに [{Slot:5,id:"minecraft:stone"}] のようにマッピングされた値、sizeに 9 の値を渡すと
		 * 配列のサイズが9、itemstack[5]にminecraft:stoneの種類を持つアイテムが格納されたItemStack配列が返されます。
		 * @param section - ItemStackを作成する情報とスロットの情報が入ったTagBaseのリス
		 * @param size -
		 * @return
		 */
		public static ItemStack[] createInventory(List<? extends TagReadable> sectionlist, int size) {
			ItemStack[] items = new ItemStack[size];
			for(int i = 0; i < sectionlist.size(); i++) {
				TagReadable base = sectionlist.get(i);
				if(base != null) items[base.get("Slot", Number.class, 0).intValue()] = createItemStack(base);
			}
			return items;
		}

		/**
		 *
		 * @param item
		 * @return
		 */
		public static TagSection createSection(ItemStack item) {
			TagSection section = new TagSection();
			if(item == null) return section;
			section.set("id", Vx_x_Rx.get().getItemMinecraft(item));
			section.set("Count", item.getAmount());
			section.set("Damage", item.getDurability());
			TagSection sec = TagTool.getSection(item);
			if(!sec.isEmpty()) section.set("nbt", sec);
			return section;
		}

		/**
		 *
		 * @param items
		 * @return
		 */
		public static List<TagSection> createSection(ItemStack[] items) {
			List<TagSection> list = new ArrayList<>();
			for(int i = 0; i < items.length; i++) {
				TagSection section = createSection(items[i]);
				if(!section.isEmpty()) {
					section.set("Slot", (byte)i);
					list.add(section);
				}
			}
			return list;
		}
	}

	private static final String VERSION = Bukkit.getServer().getClass().getPackage().getName()
			.replaceFirst(".*(\\d+_\\d+_R\\d+).*", "$1");

	//CraftItemStack-----------------------------------------------
	private static final Class<?> Class_CraftItemStack;
	private static final Method Method_asNMSCopy;
	private static final Method Field_getItemMeta;

	//	private static final Class<?> Class_CraftItemMeta;
	//	private static final Constructor<?> Instance_CraftItemMeta;

	//NMSItemStack-------------------------------------------------
	private static final Class<?> Class_NMSItemStack;
	private static final Field Field_NMSItemStack_NBTTagCompound;
	static final Field Field_NBTTagListType;

	//NBTTagCompound_Class-----------------------------------------
	private static final Class<?> Class_NBTBase;
	private static final Method Method_createTag;

	private static final Class<?> Class_NBTTagByte;
	private static final Class<?> Class_NBTTagShort;
	private static final Class<?> Class_NBTTagInt;
	private static final Class<?> Class_NBTTagLong;
	private static final Class<?> Class_NBTTagFloat;
	private static final Class<?> Class_NBTTagDouble;
	private static final Class<?> Class_NBTTagByteArray;
	private static final Class<?> Class_NBTTagString;
	static final Class<?> Class_NBTTagList;
	static final Class<?> Class_NBTTagCompound;
	private static final Class<?> Class_NBTTagIntArray;

	//NBTTagCompound_Field-----------------------------------------
	private static final Field Field_NBTTagByte;
	private static final Field Field_NBTTagShort;
	private static final Field Field_NBTTagInt;
	private static final Field Field_NBTTagLong;
	private static final Field Field_NBTTagFloat;
	private static final Field Field_NBTTagDouble;
	private static final Field Field_NBTTagByteArray;
	private static final Field Field_NBTTagString;
	static final Field Field_NBTTagList;
	private static final Field Field_NBTTagCompound;
	private static final Field Field_NBTTagIntArray;

	//NBTTagCompound_Constructor-----------------------------------------

	private static final Constructor<?> Instance_NBTTagByte;
	private static final Constructor<?> Instance_NBTTagShort;
	private static final Constructor<?> Instance_NBTTagInt;
	private static final Constructor<?> Instance_NBTTagLong;
	private static final Constructor<?> Instance_NBTTagFloat;
	private static final Constructor<?> Instance_NBTTagDouble;
	private static final Constructor<?> Instance_NBTTagByteArray;
	private static final Constructor<?> Instance_NBTTagString;
	private static final Constructor<?> Instance_NBTTagIntArray;

	//NBTTagTypeId

	static final Map<Class<?>, Class<?>> nbtclassMap = new HashMap<>();
	private static final Map<Class<?>, Class<?>> tagclassMap = new HashMap<>();
	private static final Map<Class<?>, Byte> classId = new HashMap<>();

	static {
		Class_NBTBase = fromNMSName("NBTBase");
		Method_createTag = fromName(Class_NBTBase, "createTag", byte.class);
		Method_createTag.setAccessible(true);

		Class_NBTTagByte = fromNMSName("NBTTagByte");
		Class_NBTTagShort = fromNMSName("NBTTagShort");
		Class_NBTTagInt = fromNMSName("NBTTagInt");
		Class_NBTTagLong = fromNMSName("NBTTagLong");
		Class_NBTTagFloat = fromNMSName("NBTTagFloat");
		Class_NBTTagDouble = fromNMSName("NBTTagDouble");
		Class_NBTTagByteArray = fromNMSName("NBTTagByteArray");
		Class_NBTTagString = fromNMSName("NBTTagString");
		Class_NBTTagList = fromNMSName("NBTTagList");
		Class_NBTTagCompound = fromNMSName("NBTTagCompound");
		Class_NBTTagIntArray = fromNMSName("NBTTagIntArray");

		Instance_NBTTagByte = get_Constructor(Class_NBTTagByte, byte.class);
		Instance_NBTTagShort = get_Constructor(Class_NBTTagShort, short.class);
		Instance_NBTTagInt = get_Constructor(Class_NBTTagInt, int.class);
		Instance_NBTTagLong = get_Constructor(Class_NBTTagLong, long.class);
		Instance_NBTTagFloat = get_Constructor(Class_NBTTagFloat, float.class);
		Instance_NBTTagDouble = get_Constructor(Class_NBTTagDouble, double.class);
		Instance_NBTTagByteArray = get_Constructor(Class_NBTTagByteArray, byte[].class);
		Instance_NBTTagString = get_Constructor(Class_NBTTagString, String.class);
		Instance_NBTTagIntArray = get_Constructor(Class_NBTTagIntArray, int[].class);

		Field_NBTTagByte = fromClass(Class_NBTTagByte, byte.class);
		Field_NBTTagShort = fromClass(Class_NBTTagShort, short.class);
		Field_NBTTagInt = fromClass(Class_NBTTagInt, int.class);
		Field_NBTTagLong = fromClass(Class_NBTTagLong, long.class);
		Field_NBTTagFloat = fromClass(Class_NBTTagFloat, float.class);
		Field_NBTTagDouble = fromClass(Class_NBTTagDouble, double.class);
		Field_NBTTagByteArray = fromClass(Class_NBTTagByteArray, byte[].class);
		Field_NBTTagString = fromClass(Class_NBTTagString, String.class);
		Field_NBTTagList = fromClass(Class_NBTTagList, List.class);
		Field_NBTTagCompound = fromClass(Class_NBTTagCompound, Map.class);
		Field_NBTTagIntArray = fromClass(Class_NBTTagIntArray, int[].class);

		Class_CraftItemStack = fromName("org.bukkit.craftbukkit", "inventory.CraftItemStack");
		Method_asNMSCopy = fromName(Class_CraftItemStack, "asNMSCopy", ItemStack.class);

		Class_NMSItemStack = fromName("net.minecraft.server", "ItemStack");
		Field_NMSItemStack_NBTTagCompound = fromClass(fromName("net.minecraft.server", "ItemStack"),Class_NBTTagCompound);

		Field_getItemMeta = fromName(Class_CraftItemStack, "getItemMeta", Class_NMSItemStack);
		Field_NBTTagListType = fromClass(Class_NBTTagList, byte.class);

		//Class_CraftItemMeta = fromName("org.bukkit.craftbukkit", "inventory.CraftMetaItem");
		//Instance_CraftItemMeta = get_Constructor(Class_CraftItemMeta, Class_NBTTagCompound);
	}

	private static Field fromClass(Class<?> target, Class<?> clazz) {
		putClassType(target, clazz);
		for(Field field : target.getDeclaredFields()) {
			if(field.getType().equals(clazz)) {
				field.setAccessible(true);
				return field;
			}
		}
		throw new RuntimeException(new NoSuchFieldException());
	}

	private static void putClassType(Class<?> target, Class<?> clazz) {
		try {
			if(Class_NBTBase.isAssignableFrom(target)) {
				Constructor<?> c = target.getDeclaredConstructor();
				c.setAccessible(true);
				byte type = (byte)target.getMethod("getTypeId").invoke(c.newInstance());
				classId.put(target, type);
				classId.put(clazz, type);
				nbtclassMap.put(target, clazz);
				nbtclassMap.put(clazz, clazz);
				tagclassMap.put(target, target);
				tagclassMap.put(clazz, target);
			}
		}catch(Exception e) {
			throw new RuntimeException(e);
		}

	}

	private static Constructor<?> get_Constructor(Class<?> target, Class<?>... classes) {
		try {
			return target.getConstructor(classes);
		}catch(NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}

	}

	private static Class<?> fromNMSName(String name) {
		try {
			return Class.forName("net.minecraft.server.v" + VERSION + "." + name);
		}catch(ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Class<?> fromName(String first, String last) {
		try {
			return Class.forName(first + ".v" + VERSION + "." + last);
		}catch(ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static Method fromName(Class<?> target, String name, Class<?>... classes) {
		try {
			return target.getDeclaredMethod(name, classes);
		}catch(NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	static Object get_Field(Field field, Object obj) {
		if(obj == null)
			return null;
		try {
			return field.get(obj);
		}catch(IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	static Object invoke_nmsCopy(ItemStack item) {
		try {
			return Method_asNMSCopy.invoke(item, item);
		}catch(IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	static ItemMeta invoke_itemMeta(Object nmsitem) {
		try {
			return (ItemMeta)Field_getItemMeta.invoke(nmsitem, nmsitem);
		}catch(IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	public static boolean hasTag(@Nullable ItemStack item, String tagName) {
		return hasKey(invoke_nmsCopy(item), tagName);
	}

	public static boolean hasTag(@Nullable ItemStack item, String tagName, Class<?> classFilter) {
		return hasKey(invoke_nmsCopy(item), tagName, classFilter);
	}

	public static Set<String> keySet(@Nullable ItemStack item) {
		return getNBTKeys(invoke_nmsCopy(item));
	}

	public static Class<?> getType(@Nullable ItemStack item, String tagName) {
		return getTagType(invoke_nmsCopy(item), tagName);
	}

	public static Object get(@Nullable ItemStack item, String tagName) {
		return getNBTTag(invoke_nmsCopy(item), tagName);
	}

	public static <T> T get(@Nullable ItemStack item, String tagName, Class<T> classFilter, T defaultValue) {
		return getNBTTag(invoke_nmsCopy(item), tagName, classFilter, defaultValue);
	}

	public static void set(@Nullable ItemStack item, String tagName, Object nbtTag) {
		if(item != null)item.setItemMeta(invoke_itemMeta(setNBTTag(invoke_nmsCopy(item), tagName, nbtTag)));
	}

	public static Object remove(@Nullable ItemStack item, String tagName) {
		if(item == null)return null;
		Object nmsitem = invoke_nmsCopy(item);
		Object remove = removeNBTTag(nmsitem, tagName);
		item.setItemMeta(invoke_itemMeta(nmsitem));
		return remove;
	}

	public static void clear(@Nullable ItemStack item) {
		if(item != null)
			item.setItemMeta(null);
	}

	@Deprecated

	public static Map<String, Object> getAllMap(@Nullable ItemStack item) {
		return getAllMap(invoke_nmsCopy(item));
	}

	@Deprecated

	public static void setAllMap(ItemStack item, Map<String, ?> nbtMap) {
		Object nmsitem = invoke_nmsCopy(item);
		if(nmsitem != null) {
			setAllMap(nmsitem, nbtMap);
			item.setItemMeta(invoke_itemMeta(nmsitem));
		}
	}

	public static TagSection getSection(@Nullable ItemStack item) {
		return new TagSection(item);
	}

	@Deprecated

	public static TagSection getSection(@Nullable ItemStack item, String tagName) {
		TagSection section = getSection(item);
		return section != null ? section.getSection(tagName, false) : null;
	}

	public static List<TagSection> getSectionList(@Nullable ItemStack item, String tagName) {
		TagSection section = getSection(item);
		return section.getSectionList(tagName, false);
	}

	public static TagSection createSection(@Nullable ItemStack item, String tagName) {
		TagSection section = getSection(item);
		return section.createSection(tagName);
	}

	/*
	 * NMSItemStack -> NBTTagTool
	 * NMSのアイテムからNBTを色々前(後)処理するところ。
	 */

	@Nonnull
	private static Set<String> getNBTKeys(Object nmsitem) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)
			return new HashSet<>(0);
		Set<String> key = new HashSet<>(map.size());
		map.forEach((x, y) -> key.add(x.toString()));
		return key;
	}

	private static boolean hasKey(Object nmsitem, String key) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null && map.containsKey(key);
	}

	private static boolean hasKey(Object nmsitem, String key, Class<?> classFilter) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null && map.containsKey(key) && checkClass(nbtclassMap.get(map.get(key).getClass()), classFilter);
	}

	@Nullable
	private static Class<?> getTagType(Object nmsitem, String key) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null && map.containsKey(key) ? nbtclassMap.get(map.get(key).getClass()) : null;
	}

	@Nullable
	private static Object getNBTTag(Object nmsitem, String tagName) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null ? getNBTTag(map.get(tagName)) : null;
	}

	@Nullable
	private static <T> T getNBTTag(Object nmsitem, String tagName, Class<T> classFilter, T defaultValue) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)
			return defaultValue;
		Object tagObject = map.get(tagName);
		return tagObject != null && checkClass(nbtclassMap.get(tagObject.getClass()), classFilter)
				? classFilter.cast(getNBTTag(tagObject))
				: defaultValue;
	}

	@Nullable
	private static <T> List<T> putNBTTag(Object nmsitem, String tagName, Class<T> classFilter, List<T> putList) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null)
			return putList;
		Object tagObject = getNBTTag(map.get(tagName));
		if(tagObject instanceof Collection)
			for(Object tag : (Collection<?>)tagObject) {
			if(classFilter.isInstance(tag)) putList.add(classFilter.cast(tag));
			}
		return putList;
	}

	@Nullable
	private static <T> Map<String, T> putNBTTag(Object nmsitem, String tagName, Class<T> classFilter,
			Map<String, T> putMap) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null) return putMap;
		Object tagObject = getNBTTag(map.get(tagName));
		if(tagObject instanceof Map)
			for(Entry<?, ?> entry : ((Map<?, ?>)tagObject).entrySet()) {
			if(classFilter.isInstance(entry.getValue()))
				putMap.put(entry.getKey().toString(), classFilter.cast(entry.getValue()));
			}
		return putMap;
	}

	@Nonnull
	private static Map<Object, Object> getNBTCopy(Object nmsitem) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map == null ? new HashMap<>() : new HashMap<>(map);
	}

	@Nullable
	static Map<?, ?> getNBTMirror(Object nmsitem) {
		Object NBTTagCompound = get_Field(Field_NMSItemStack_NBTTagCompound, nmsitem);
		return (Map<?, ?>)get_Field(Field_NBTTagCompound, NBTTagCompound);
	}

	@Nullable
	@SuppressWarnings("unchecked")
	static Map<String, Object> getEditableNBTMirror(Object NBTTagCompound) {
		return (Map<String, Object>)get_Field(Field_NBTTagCompound, NBTTagCompound);
	}

	@Nullable
	private static Object setNBTTag(Object nmsitem, String tagName, Object tagObject) {
		Map<Object, Object> map = getNBTCopy(nmsitem);
		map.put(tagName, tagObject);
		setNBTCompound(nmsitem, map);
		return nmsitem;
	}

	private static Object removeNBTTag(Object nmsitem, String tagName) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		return map != null ? map.remove(tagName) : null;
	}

	@Nonnull
	private static Map<String, Object> getAllMap(Object nmsitem) {
		Map<?, ?> map = getNBTMirror(nmsitem);
		if(map == null) return new HashMap<>(0);
		Map<String, Object> newmap = new HashMap<>(map.size());
		map.forEach((x, y) -> newmap.put(x.toString(), getNBTTag(y)));
		return newmap;
	}

	private static void setAllMap(Object nmsitem, Map<String, ?> nbtMap) {
		setNBTCompound(nmsitem, nbtMap);
	}

	/*
	 * NMSItemStack -> NBTCompound
	 * NMSのアイテムから最浅部のみで変換したNBTタグを取得するところ。
	 */

	static byte getTagTypeId(Class<?> input) {
		if(input == null) return -1;
		for(Entry<Class<?>, Byte> entry : classId.entrySet()) {
			if(entry.getKey().isAssignableFrom(input)) return entry.getValue();
		}
		return -1;
	}

	/*
	 * NMSItemStack -> NBTCompound
	 * NMSのアイテムからMap型のNBTをCompoundに変換しNBTタグに書き込むところ。
	 */

	static void setNBTCompound(Object nmsitem, Object map) {
		try {
			Field_NMSItemStack_NBTTagCompound.set(nmsitem, setNBTTag(map));
		}catch(IllegalAccessException e) {
			throw new RuntimeException(e);
		}catch(NullPointerException e) {
			throw new IllegalArgumentException("The material is not compatible \"net.minecraft.server.v" + VERSION + ".ItemStack\" [" + nmsitem + "]");
		}
	}

	static boolean checkClass(Class<?> base, Class<?> check) {
		return check == base || check.isAssignableFrom(check) ||
				(check == Byte.class && base == byte.class) ||
				(check == Short.class && base == short.class) ||
				(check == Integer.class && base == int.class) ||
				(check == Long.class && base == long.class) ||
				(check == Float.class && base == float.class) ||
				(check == Double.class && base == double.class) ||
				(check == Boolean.class && base == boolean.class) ||
				(check == Character.class && base == char.class);
	}

	/*
	 * NBTBase -> Object
	 * 変換するところ。
	 */

	@Nullable
	static Object getNBTTag(Object NBTTagCompound) {
		if(NBTTagCompound == null) return null;
		Byte type = classId.get(NBTTagCompound.getClass());
		if(type == null) return null;
		try {
			switch(type) {
			case 1:
				return Field_NBTTagByte.get(NBTTagCompound);
			case 2:
				return Field_NBTTagShort.get(NBTTagCompound);
			case 3:
				return Field_NBTTagInt.get(NBTTagCompound);
			case 4:
				return Field_NBTTagLong.get(NBTTagCompound);
			case 5:
				return Field_NBTTagFloat.get(NBTTagCompound);
			case 6:
				return Field_NBTTagDouble.get(NBTTagCompound);
			case 7:
				return Field_NBTTagByteArray.get(NBTTagCompound);
			case 11:
				return Field_NBTTagIntArray.get(NBTTagCompound);
			case 8:
				return Field_NBTTagString.get(NBTTagCompound);
			case 9:
				List<?> lists = ((List<?>)Field_NBTTagList.get(NBTTagCompound));
				List<Object> list = new ArrayList<>(lists.size());
				lists.forEach(x -> list.add(getNBTTag(x)));
				return list;
			case 10:
				Map<?, ?> maps = ((Map<?, ?>)Field_NBTTagCompound.get(NBTTagCompound));
				Map<String, Object> map = new HashMap<>(maps.size());
				maps.forEach((x, y) -> map.put(x.toString(), getNBTTag(y)));
				return map;

			default:
				throw new RuntimeException();
			}
		}catch(IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * Object -> NBTBase
	 * 変換するところ。
	 */

	@Nullable
	static Object setNBTTag(Object tagObject) {
		try {
			if(tagObject == null || Class_NBTBase.isInstance(tagObject)) {
				return tagObject;
			}else if(tagObject instanceof Boolean) {
				return Instance_NBTTagByte.newInstance(Byte.valueOf(tagObject.equals(true) ? (byte)1 : 0));
			}else if(tagObject instanceof Byte) {
				return Instance_NBTTagByte.newInstance(tagObject);
			}else if(tagObject instanceof Short) {
				return Instance_NBTTagShort.newInstance(tagObject);
			}else if(tagObject instanceof Integer) {
				return Instance_NBTTagInt.newInstance(tagObject);
			}else if(tagObject instanceof Long) {
				return Instance_NBTTagLong.newInstance(tagObject);
			}else if(tagObject instanceof Float) {
				return Instance_NBTTagFloat.newInstance(tagObject);
			}else if(tagObject instanceof Double) {
				return Instance_NBTTagDouble.newInstance(tagObject);
			}else if(tagObject instanceof byte[] || tagObject instanceof Byte[]) {
				return Instance_NBTTagByteArray.newInstance(tagObject);
			}else if(tagObject instanceof int[] || tagObject instanceof Integer[]) {
				return Instance_NBTTagIntArray.newInstance(tagObject);
			}else if(tagObject instanceof String) {
				return Instance_NBTTagString.newInstance(tagObject);
			}else if(tagObject instanceof List) {
				Object nbttag = Class_NBTTagList.newInstance();
				List<Object> list = new ArrayList<>(((List<?>)tagObject).size());
				Class<?> firstclass = null;
				for(Object array : (List<?>)tagObject) {
					if(array != null && firstclass == null) {
						firstclass = array != null && firstclass == null ? array.getClass() : firstclass;
						break;
					}
				}
				byte type = getTagTypeId(firstclass);
				for(Object array : (List<?>)tagObject) {
					if(array != null && !firstclass.isInstance(array)) {
						ClassCastException e = new ClassCastException(array.getClass() + " can not cast " + firstclass);
						Bukkit.getLogger().log(Level.SEVERE, "配列内のクラスは1種類である必要があります。 ", e);
						throw e;
					}
					list.add(array == null ? Method_createTag.invoke(type, type) : setNBTTag(array));
				}
				Field_NBTTagListType.set(nbttag, type);
				Field_NBTTagList.set(nbttag, list);
				return nbttag;
			}else if(tagObject instanceof Map) {
				Object nbttag = Class_NBTTagCompound.newInstance();
				Map<String, Object> map = new HashMap<>(((Map<?, ?>)tagObject).size());
				for(Entry<?, ?> entry : ((Map<?, ?>)tagObject).entrySet()) {
					Object value = setNBTTag(entry.getValue());
					if(value != null) map.put(entry.getKey().toString(), value);
				}
				Field_NBTTagCompound.set(nbttag, map);
				return nbttag;
			}else if(tagObject instanceof TagBase) {
				Object nbttag = Class_NBTTagCompound.newInstance();
				Map<String, Object> map = new HashMap<>(((TagBase)tagObject).size());
				for(Entry<?, ?> entry : ((TagBase)tagObject).toMap().entrySet()) {
					Object value = setNBTTag(entry.getValue());
					if(value != null) map.put(entry.getKey().toString(), value);
				}
				Field_NBTTagCompound.set(nbttag, map);
				return nbttag;
			}
			ClassCastException e = new ClassCastException(tagObject.getClass() + " can not cast NBTBase");
			Bukkit.getLogger().log(Level.SEVERE, "NBTに変換できる値を入力してください。 ", e);
			throw e;
		}catch(InstantiationException | IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
}
