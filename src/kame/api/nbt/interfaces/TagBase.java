package kame.api.nbt.interfaces;

import java.util.Map;
import java.util.Set;

/**
 * このインターフェースはNBTタグの最低限の機能を提供するインターフェースです。
 * @author かめめ
 */
public interface TagBase {

	/**
	 * 指定されたキーがこのマップに含まれている場合にtrueを返します。
	 * (このようなマッピングは1つのみ存在できます。)
	 * @param tagName - このマップ内にあるかどうかが判定されるキー
	 * @return 指定されたキーに値が含まれていた場合true
	 */
	public boolean hasTag(String tagName);

	/**
	 * 指定されたキーがこのマップに含まれており、かつ指定クラスにキャストできる場合にtrueを返します。
	 * @param tagName - このマップ内にあるかどうかが判定されるキー
	 * @param classFilter - 判定されるキーのクラス
	 * @return 指定されたキーに値が含まれており、かつ指定クラスにキャストできる場合true
	 */
	public boolean hasTag(String tagName, Class<?> classFilter);

	/**
	 * このマップに含まれるキーのSetビューを返します。
	 * セットはマップと連動しているので、マップに対する変更はセットに反映され、また、セットに対する変更はマップに反映されます。
	 * セットの反復処理中にマップが変更された場合、反復処理の結果は定義されていません(イテレータ自身のremoveオペレーションを除く)。
	 * セットは要素の削除をサポートします。Iterator.remove、Set.remove、removeAll、retainAll、およびclearオペレーションで対応するマッピングをマップから削除します。
	 * addまたはaddAllオペレーションはサポートしていません。
	 * @return マップに含まれているキーのセット・ビュー
	 */
	public Set<String> keySet();

	/**
	 * このNBTの階層がキーと値のマッピングを保持しない場合にtrueを返します。
	 * @return このマップがキーと値のマッピングを保持しない場合はtrue
	 */
	public boolean isEmpty();

	/**
	 * このNBTの階層内の要素数を返します。NBTの階層内にInteger.MAX_VALUEより多くの要素がある場合は、Integer.MAX_VALUEを返します。
	 * @return このNBTの階層内の要素数
	 */
	public int size();

	/**
	 * このNBTが持っている要素をマップに変換して返します。
	 * @return このNBTが持っているすべての要素
	 */
	public Map<String, Object> toMap();
}
