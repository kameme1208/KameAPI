package kame.api.nbt.interfaces;

import java.util.List;

import kame.api.nbt.interfaces.TagBase;

/**
 * このインターフェースはNBTタグの読み出し機能を提供するインターフェースです。
 * @author かめめ
 */
public interface TagReadable extends TagBase {

	/**
	 * 指定されたキーがマップされている値を返します。このマップにそのキーのマッピングが含まれていない場合はnullを返します。
	 * @param tagName - 関連付けられた値が返されるキー
	 * @return 指定されたキーがマップされている値。このマップにそのキーのマッピングが含まれていない場合はnull
	 */
	public Object get(String tagName);

	/**
	 * 指定されたキーがマップされている値を返します。このマップにそのキーのマッピングが含まれていない、または指定のクラスと一致しない場合はdefaultValueで渡された値を返します。
	 * @param tagName - 関連付けられた値が返されるキー
	 * @param classFilter - 関連付けられるキーの指定クラス
	 * @param defaultValue - 関連付けられた要素が無かったときに返す値
	 * @return 指定されたキーがマップされている値。このマップにそのキーのマッピングが含まれていない場合はdefaultValue
	 */
	public <T> T get(String tagName, Class<T> classFilter, T defaultValue);

	/**
	 * 指定されたキーがマップされているList配列のコピーを返します。配列内のクラスが指定のクラスと一致した要素のみ渡されたList配列に代入されます。
	 * @param tagName - 関連付けられた値が返されるキー
	 * @param classFilter - 関連付けられるキーの要素ないの指定クラス
	 * @param putList - 関連付けられた配列がコピーされるList配列
	 * @return 指定されたキーがマップされている値をputListにコピーした値。
	 */
	public <T> List<T> list(String tagName, Class<T> classFilter, List<T> putList);

	/**
	 * 指定されたキーがマップされているNBTの階層を返します。配列内のクラスがMap.classと一致しない場合はreturnEmptyの値がtrueの場合だけ空のインスタンスが返されます。
	 * @param tagName - 関連付けられた値が返されるキー
	 * @param classFilter - 関連付けられるキーの要素ないの指定クラス
	 * @param putList - 関連付けられた配列がコピーされるList配列
	 * @return 指定されたキーがマップされている値をputListにコピーした値。
	 */
	public TagBase getSection(String tagName, boolean returnEmpty);

	/**
	 * 指定されたキーがマップされているNBTの階層の配列を返します。配列内のクラスがList<Map<String, Object>と一致しない場合はreturnEmptyの値がtrueの場合だけ空のインスタンスが返されます。
	 * @param tagName - 関連付けられた値が返されるキー
	 * @param classFilter - 関連付けられるキーの要素ないの指定クラス
	 * @param putList - 関連付けられた配列がコピーされるList配列
	 * @return 指定されたキーがマップされている値をputListにコピーした値。
	 */
	public List<? extends TagBase> getSectionList(String tagName, boolean returnEmpty);

	/**
	 * 指定されたキーに格納されている要素のクラスを取得します。
	 * @param tagName - 関連付けられた値のクラスが返されるキー
	 * @return 関連付けられている要素のクラス
	 */
	public Class<?> getType(String tagName);
}
