package kame.api.nbt.interfaces;

import java.util.List;

/**
 * このインターフェースはNBTタグの書き込み機能を提供するインターフェースです。
 * @author かめめ
 */
public interface TagWriteable extends TagBase {

	/**
	 * 指定したキーに新しくNBTの階層を作成します。
	 * @param tagName - NBTの階層を作成するキー値
	 * @return 新しく作成したNBTの階層
	 */
	public abstract TagWriteable createSection(String tagName);

	/**
	 * 指定したキーに新しくNBTの階層のList配列を作成します。
	 * @param tagName - NBTの階層のList配列を作成するキー値
	 * @return 新しく作成したNBTの階層のList配列
	 */
	public abstract List<? extends TagWriteable> createSectionList(String tagName);

	/**
	 * このNBTの指定されたキーに要素を格納します。
	 * 指定したキーに値を格納します。
	 * @param tagName - 格納するキーの値
	 * @param nbt - 格納する要素の値
	 */
	public abstract TagWriteable set(String tagName, Object nbt);

	/**
	 * このNBTの要素を渡されたNBTの要素で置き換えます。
	 * @param nbt - 置き換えるNBTの要素
	 */
	public abstract TagWriteable setSection(TagBase nbt);

	/**
	 * 指定されたキーがこのNBTの要素にあれば、その最初のものをNBTの要素から削除します。その要素がない場合、変更はありません。
	 * @param tagName - このNBTの要素から削除される要素(その要素が存在する場合)
	 * @return 指定されたキーに含まれていた値
	 */
	public abstract Object remove(String tagName);

}
