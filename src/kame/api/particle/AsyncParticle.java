package kame.api.particle;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.bukkit.util.Vector;

import kame.api.position.Circle3D;
import kame.api.position.Line3D;
import kame.api.position.Point3D;

/**
 * @author kameme
 *
 */
public abstract class AsyncParticle {

	/**
	 * 非同期のスレッドでパーティクルを表示させます
	 */
	public abstract void draw();

	/**
	 * パーティクルを表示させます
	 * @param thread trueなら別スレッドでパーティクルを表示させます。
	 * 表示間隔を1以上に設定した状態で引数にfalseを渡すことは非推奨です。
	 */
	public abstract void draw(boolean thread);

	/**
	 * パーティクルの常時間隔を設定します
	 * @param millis 表示間隔のミリ秒
	 */
	public abstract void setDrawSpeed(long millis);

	/**
	 * このインスタンスのパーティクルを表示させるプレイヤーを追加します。
	 * @param player 追加する対象のプレイヤー
	 */
	public abstract void addSend(Player player);

	/**
	 * このインスタンスのパーティクルを表示させるプレイヤーを追加します。
	 * @param player 追加する対象のプレイヤー
	 */
	public abstract void addSend(Player... players);

	/**
	 * このインスタンスのパーティクルを表示させるプレイヤーを追加します。
	 * @param player 追加する対象のプレイヤー
	 */
	public abstract void addSend(Collection<? extends Player> players);

	/**
	 * 点を描画するパーティクルのインスタンスを作成します。
	 * @param particle - パーティクルの種類
	 * @param loc - 座標
	 * @param motion - パーティクルの動く向き・色など
	 * @param amount - 表示数
	 * @param speed - 表示速度・移動速度など
	 * @param data - ブロックの種類やデータ値
	 * @return
	 */
	public static AsyncParticle dot(Particle particle, Location loc, Vector motion, int amount, double speed, MaterialData data) {
		return new AbstractAsyncParticle.AsyncParticleDot(particle, loc, motion, amount, speed, data);
	}

	/**
	 * 2つの座標値から線を描画するパーティクルのインスタンスを作成します。
	 * @param particle - パーティクルの種類
	 * @param from - 始点
	 * @param to - 終点
	 * @param motion - パーティクルの動く向き・色など
	 * @param amount - 表示数
	 * @param speed - 表示速度・移動速度など
	 * @param data - ブロックの種類やデータ値
	 * @return
	 */
	public static AsyncParticle line(Particle particle, Location from, Location to, Vector motion, int amount, double speed, MaterialData data) {
		return new AbstractAsyncParticle.AsyncParticleLine(particle, from, to, motion, amount, speed, data);
	}

	/**
	 * 2つの座標ベクトルから線を描画するパーティクルのインスタンスを作成します。
	 * @param particle - パーティクルの種類
	 * @param from - 始点
	 * @param to - 終点
	 * @param motion - パーティクルの動く向き・色など
	 * @param amount - 表示数
	 * @param speed - 表示速度・移動速度など
	 * @param data - ブロックの種類やデータ値
	 * @return
	 */
	public static AsyncParticle line(Particle particle, Vector from, Vector to, Vector motion, int amount, double speed, MaterialData data) {
		return new AbstractAsyncParticle.AsyncParticleLine(particle, from, to, motion, amount, speed, data);
	}

	/**
	 * 中心座標とその座標が持つ方向と半径から円を描画するパーティクルのインスタンスを作成します。
	 * @param particle - パーティクルの種類
	 * @param loc - 中心座標と始点の方向
	 * @param radius - 半径
	 * @param motion - パーティクルの動く向き・色など
	 * @param amount - 表示数
	 * @param speed - 表示速度・移動速度など
	 * @param offset
	 * @param data - ブロックの種類やデータ値
	 * @return
	 */
	public static AsyncParticle circle(Particle particle, Location loc, double radius, Vector motion, int amount, double speed, double offset, MaterialData data) {
		return new AbstractAsyncParticle.AsyncParticleCircle(particle, loc, radius, motion, amount, speed, offset, data);
	}

	/**
	 * 始点と終点、その2つの持つ方向と距離から4次ベジェ曲線を描画するパーティクルのインスタンスを作成します。
	 * @param particle - パーティクルの種類
	 * @param from - 始点
	 * @param from_length - 始点の持つ方向からのP1とする距離
	 * @param to - 終点
	 * @param to_length - 終点の持つ方向からP2とする距離
	 * @param motion - パーティクルの動く向き・色など
	 * @param amount - 表示数
	 * @param speed - 表示速度・移動速度など
	 * @param data - ブロックの種類やデータ値
	 * @return
	 */
	public static AsyncParticle bezier(Particle particle, Location from, double from_length, Location to, double to_length, Vector motion, int amount, double speed, MaterialData data) {
		return new AbstractAsyncParticle.AsyncParticleBezier(particle, from, from.clone().add(from.getDirection().multiply(from_length)), to, to.clone().add(to.getDirection().multiply(from_length)), motion, amount, speed, data);
	}

	/**
	 * P0,P1,P2,P3から4次ベジェ曲線を描画するパーティクルのインスタンスを作成します。
	 * @param particle - パーティクルの種類
	 * @param from - 始点(P0)
	 * @param from_length - P1の座標
	 * @param to - 終点(P3)
	 * @param to_length - P2の座標
	 * @param motion - パーティクルの動く向き・色など
	 * @param amount - 表示数
	 * @param speed - 表示速度・移動速度など
	 * @param data - ブロックの種類やデータ値
	 * @return
	 */
	public static AsyncParticle bezier(Particle particle, Location from, Location from_length, Location to, Location to_length, Vector motion, int amount, double speed, MaterialData data) {
		return new AbstractAsyncParticle.AsyncParticleBezier(particle, from, from_length, to, to_length, motion, amount, speed, data);
	}

	/**
	 * P0,P1,P2,P3から4次ベジェ曲線を描画するパーティクルのインスタンスを作成します。
	 * @param particle - パーティクルの種類
	 * @param from - 始点(P0)
	 * @param from_length - P1の座標
	 * @param to - 終点(P3)
	 * @param to_length - P2の座標
	 * @param motion - パーティクルの動く向き・色など
	 * @param amount - 表示数
	 * @param speed - 表示速度・移動速度など
	 * @param data - ブロックの種類やデータ値
	 * @return
	 */
	public static AsyncParticle bezier(Particle particle, Vector from, Vector from_length, Vector to, Vector to_length, Vector motion, int amount, double speed, MaterialData data) {
		return new AbstractAsyncParticle.AsyncParticleBezier(particle, from, from_length, to, to_length, motion, amount, speed, data);
	}

	private static abstract class AbstractAsyncParticle extends AsyncParticle implements Runnable {
		protected Set<Player>	players	= new HashSet<>(0);
		protected Particle		particle;
		protected Vector		mov;
		protected int			amount;
		protected double		speed;
		protected MaterialData	data;
		protected long			drawSpeed;

		public AbstractAsyncParticle(Particle particle, Vector motion, int amount, double speed, MaterialData data) {
			this.particle = particle;
			this.mov = motion;
			this.amount = amount;
			this.speed = speed;
			this.data = data;
		}

		@Override
		public void setDrawSpeed(long millis) {
			this.drawSpeed = millis;
		}

		@Override
		public void draw() {
			new Thread(this).start();
		}

		@Override
		public void draw(boolean thread) {
			if(thread)
				draw();
			else
				run();
		}

		@Override
		public void addSend(Player player) {
			players.add(player);
		}

		@Override
		public void addSend(Player... player) {
			for(Player p : player)
				players.add(p);
		}

		@Override
		public void addSend(Collection<? extends Player> player) {
			for(Player p : player)
				players.add(p);
		}

		protected void delay(long millis) {
			if(millis > 0)
				try {
					Thread.sleep(millis);
				}catch (InterruptedException e) {
					e.printStackTrace();
				}
		}

		private static class AsyncParticleDot extends AbstractAsyncParticle {
			private final Location loc;

			private AsyncParticleDot(Particle particle, Location loc, Vector motion, int amount, double speed, MaterialData data) {
				super(particle, motion, amount, speed, data);
				this.loc = loc;
			}

			@Override
			public void run() {
				if(drawSpeed != 0) {
					int a = Math.abs(amount);
					for(int i = a; i != 0; i--) {
						players.forEach(x -> x.spawnParticle(particle, loc, amount > 0 ? 1 : 0, mov.getX(), mov.getY(), mov.getZ(), speed, data == null
								|| data.getItemType().isBlock() ? data : data.toItemStack(1)));
						delay(drawSpeed);
					}
				}else {
					players.forEach(x -> x.spawnParticle(particle, loc, amount, mov.getX(), mov.getY(), mov.getZ(), speed, data == null
							|| data.getItemType().isBlock() ? data : data.toItemStack(1)));
				}
			}

		}

		private static class AsyncParticleLine extends AbstractAsyncParticle {
			private final Line3D line;

			private AsyncParticleLine(Particle particle, Location from, Location to, Vector motion, int amount, double speed, MaterialData data) {
				super(particle, motion, amount, speed, data);
				this.line = new Line3D(from, to);
			}

			private AsyncParticleLine(Particle particle, Vector from, Vector to, Vector motion, int amount, double speed, MaterialData data) {
				super(particle, motion, amount, speed, data);
				this.line = new Line3D(from, to);
			}

			@Override
			public void run() {
				double a = Math.abs(amount);
				for(int i = 0; i < a; i++) {
					Point3D vec = line.getPoint(i / a);
					for(Player x : players)
						x.spawnParticle(particle, vec.getX(), vec.getY(), vec.getZ(), amount > 0 ? 1 : 0, mov.getX(), mov.getY(), mov.getZ(), speed, data == null
								|| data.getItemType().isBlock() ? data : data.toItemStack(1));
					delay(drawSpeed);
				}

			}

		}

		private static class AsyncParticleCircle extends AbstractAsyncParticle {
			private final Circle3D	circle;
			private final double	offset;

			public AsyncParticleCircle(Particle particle, Location loc, double radius, Vector motion, int amount, double speed, double offset, MaterialData data) {
				super(particle, motion, amount, speed, data);
				this.circle = new Circle3D(loc, radius);
				this.offset = offset;
			}

			@Override
			public void run() {
				double a = Math.abs(amount);
				for(int i = 0; i < a; i++) {
					Point3D vec = circle.getPoint(360 * (i / a) + offset);
					for(Player x : players)
						x.spawnParticle(particle, vec.getX(), vec.getY(), vec.getZ(), amount > 0 ? 1 : 0, mov.getX(), mov.getY(), mov.getZ(), speed, data == null
								|| data.getItemType().isBlock() ? data : data.toItemStack(1));
					delay(drawSpeed);
				}
			}

		}

		private static class AsyncParticleBezier extends AbstractAsyncParticle {
			private final Line3D	point1;
			private final Line3D	point3;

			private AsyncParticleBezier(Particle particle, Location from1, Location from2, Location to1, Location to2, Vector motion, int amount, double speed, MaterialData data) {
				super(particle, motion, amount, speed, data);
				point1 = new Line3D(from1, from2);
				point3 = new Line3D(to1, to2);
			}

			private AsyncParticleBezier(Particle particle, Vector from1, Vector from2, Vector to1, Vector to2, Vector motion, int amount, double speed, MaterialData data) {
				super(particle, motion, amount, speed, data);
				point1 = new Line3D(from1, from2);
				point3 = new Line3D(to1, to2);
			}

			@Override
			public void run() {
				double a = Math.abs(amount);
				for(int i = 0; i < a; i++) {
					Point3D vec = point1.getBezier3(point3, i / a);
					for(Player x : players)
						x.spawnParticle(particle, vec.getX(), vec.getY(), vec.getZ(), amount > 0 ? 1 : 0, mov.getX(), mov.getY(), mov.getZ(), speed, data == null
								|| data.getItemType().isBlock() ? data : data.toItemStack(1));
					delay(drawSpeed);
				}
			}
		}
	}

}
