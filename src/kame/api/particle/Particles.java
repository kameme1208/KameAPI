package kame.api.particle;

import org.bukkit.Particle;

public enum Particles {
	angryvillager(Particle.VILLAGER_ANGRY),
	barrier(Particle.BARRIER),
	blockcrack(Particle.BLOCK_CRACK),
	blockdust(Particle.BLOCK_DUST),
	bubble(Particle.WATER_BUBBLE),
	cloud(Particle.CLOUD),
	crit(Particle.CRIT),
	damageindicator(Particle.DAMAGE_INDICATOR),
	depthsuspend(Particle.SUSPENDED_DEPTH),
	dragonbreath(Particle.DRAGON_BREATH),
	driplava(Particle.DRIP_LAVA),
	dripwater(Particle.DRIP_WATER),
	droplet(Particle.WATER_DROP),
	enchantmenttable(Particle.ENCHANTMENT_TABLE),
	endrod(Particle.END_ROD),
	explode(Particle.EXPLOSION_NORMAL),
	fallingdust(Particle.FALLING_DUST),
	fireworksspark(Particle.FIREWORKS_SPARK),
	flame(Particle.FLAME),
	footstep(Particle.FOOTSTEP),
	happyvillager(Particle.VILLAGER_HAPPY),
	heart(Particle.HEART),
	hugeexplosion(Particle.EXPLOSION_HUGE),
	iconcrack(Particle.ITEM_CRACK),
	instantspell(Particle.SPELL_INSTANT),
	largeexplode(Particle.EXPLOSION_LARGE),
	largesmoke(Particle.SMOKE_LARGE),
	lava(Particle.LAVA),
	magiccrit(Particle.CRIT_MAGIC),
	mobappearance(Particle.MOB_APPEARANCE),
	mobspell(Particle.SPELL_MOB),
	mobspellambient(Particle.SPELL_MOB_AMBIENT),
	note(Particle.NOTE),
	portal(Particle.PORTAL),
	reddust(Particle.REDSTONE),
	slime(Particle.SLIME),
	smoke(Particle.SMOKE_NORMAL),
	snowballpoof(Particle.SNOWBALL),
	snowshovel(Particle.SNOW_SHOVEL),
	spell(Particle.SPELL),
	splash(Particle.WATER_SPLASH),
	suspended(Particle.SUSPENDED),
	sweepattack(Particle.SWEEP_ATTACK),
	take(Particle.ITEM_TAKE),
	townaura(Particle.TOWN_AURA),
	wake(Particle.WATER_WAKE),
	witchmagic(Particle.SPELL_WITCH),
	;

	private Particle bukkitparticle;

	private Particles(Particle particle) {
		this.bukkitparticle = particle;
	}

	public Particle toBukkitParticle() {
		return this.bukkitparticle;
	}
}
