package kame.api.position;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Circle3D {
	private Point3D	point;
	private double	yaw, pitch, radius;

	public Circle3D(Location center, double radius) {
		point = new Point3D(center);

		this.yaw = center.getYaw();
		this.pitch = center.getPitch();
		this.radius = radius;
	}

	public Circle3D(Vector center, double yaw, double pitch, double radius) {
		point = new Point3D(center);

		this.yaw = yaw;
		this.pitch = pitch;
		this.radius = radius;
	}

	public Circle3D(Point3D center, double yaw, double pitch, double radius) {
		point = new Point3D(center);

		this.yaw = yaw;
		this.pitch = pitch;
		this.radius = radius;
	}

	public Circle3D(double x, double y, double z, double yaw, double pitch, double radius) {
		point = new Point3D(x, y, z);

		this.yaw = yaw;
		this.pitch = pitch;
		this.radius = radius;
	}

	public Point3D getPoint(double degree) {
		return point.getPoint(yaw, pitch, degree, 0, radius, 0);
	}

}
