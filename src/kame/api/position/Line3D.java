package kame.api.position;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Line3D {

	private double fx, fy, fz, tx, ty, tz;

	public Line3D(Location from, Location to) {
		fx = from.getX();
		fy = from.getY();
		fz = from.getZ();

		tx = to.getX();
		ty = to.getY();
		tz = to.getZ();
	}

	public Line3D(Vector from, Vector to) {
		fx = from.getX();
		fy = from.getY();
		fz = from.getZ();

		tx = to.getX();
		ty = to.getY();
		tz = to.getZ();
	}

	public Line3D(Point3D from, Point3D to) {
		fx = from.getX();
		fy = from.getY();
		fz = from.getZ();

		tx = to.getX();
		ty = to.getY();
		tz = to.getZ();
	}

	public Line3D(double fx, double fy, double fz, double tx, double ty, double tz) {
		this.fx = fx;
		this.fy = fy;
		this.fz = fz;

		this.tx = tx;
		this.ty = ty;
		this.tz = tz;
	}

	public Point3D getPoint(double point) {
		return new Point3D(fx + (tx - fx) * point, fy + (ty - fy) * point, fz + (tz - fz) * point);
	}

	public Point3D getBezier2(Point3D line, double point) {
		double p = 1 - point;
		double a = p * p;
		double b = 2 * p * point;
		double c = point * point;
		return new Point3D(a * fx + b * tx + c * line.getX(), a * fy + b * ty + c * line.getY(), a * fz + b * tz
				+ c * line.getZ());
	}

	public Point3D getBezier3(Line3D line, double point) {
		double p = 1 - point;
		double a = p * p * p;
		double b = 3 * p * p * point;
		double c = 3 * p * point * point;
		double d = point * point * point;
		return new Point3D(a * fx + b * tx + c * line.fx + d * line.tx, a * fy + b * ty + c * line.fy
				+ d * line.ty, a * fz + b * tz + c * line.fz + d * line.tz);
	}
}
