package kame.api.position;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Point3D {

	private static final double pi2 = 3.14159265358979323846 / 180;

	private double x, y, z;

	public Point3D(Location point) {
		x = point.getX();
		y = point.getY();
		z = point.getZ();
	}

	public Point3D(Vector point) {
		x = point.getX();
		y = point.getY();
		z = point.getZ();
	}

	public Point3D(Point3D point) {
		x = point.getX();
		y = point.getY();
		z = point.getZ();
	}

	public Point3D(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Point3D add(Point3D point) {
		this.x += point.x;
		this.y += point.y;
		this.z += point.z;
		return this;
	}

	public Point3D add(double x, double y, double z) {
		this.x += x;
		this.y += y;
		this.z += z;
		return this;
	}

	public Point3D getPoint(double x, double y, double z) {
		return new Point3D(this.x + x, this.y + y, this.z + z);
	}

	public Point3D getPoint(double yaw, double pitch, double roll, double x, double y, double z) {
		return new Point3D(x, y, z).rotate(yaw, pitch, roll).add(this);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	public double setX(double x) {
		return this.x = x;
	}

	public double setY(double y) {
		return this.y = y;
	}

	public double setZ(double z) {
		return this.z = z;
	}

	public Vector toVector() {
		return new Vector(x, y, z);
	}

	public double distance(Point3D point) {
		return Math.sqrt((this.x - point.x) * (this.x - point.x) + (this.y - point.y) * (this.y - point.y)
				+ (this.z - point.z) * (this.z - point.z));
	}

	public double distanceSquared(Point3D point) {
		return (this.x - point.x) * (this.x - point.x) + (this.y - point.y) * (this.y - point.y)
				+ (this.z - point.z) * (this.z - point.z);
	}

	public Point3D rotate(double yaw, double pitch, double roll) {
		if(roll != 0) {
			double old = this.x;
			double sin = Math.sin(roll * pi2);
			double cos = Math.cos(roll * pi2);
			this.x = old * cos - this.y * sin;
			this.y = old * sin + this.y * cos;
		}
		if(pitch != 0) {
			double old = this.z;
			double sin = Math.sin(-pitch * pi2);
			double cos = Math.cos(-pitch * pi2);
			this.z = old * cos - this.y * sin;
			this.y = old * sin + this.y * cos;
		}
		if(yaw != 0) {
			double old = this.x;
			double sin = Math.sin(yaw * pi2);
			double cos = Math.cos(yaw * pi2);
			this.x = old * cos - this.z * sin;
			this.z = old * sin + this.z * cos;
		}
		return this;
	}

	public Point3D getRotate(double yaw, double pitch, double roll) {
		return new Point3D(x, y, z).rotate(yaw, pitch, roll);
	}
}
