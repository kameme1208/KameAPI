package kame.api.position;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import kame.api.baseapi.Vx_x_Rx;

public class Teleport {

	public static void teleport(Player player, Location loc, Set<TeleportFlag> flag) {
		Vx_x_Rx.get().teleport(player, loc, flag);
	}

	public enum TeleportFlag {
		X, Y, Z, PITCH, YAW
	}

}
